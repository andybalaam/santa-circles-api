## Deployment notes

After running `make deploy`:

```bash
ssh santacirclesapi.artificialworlds.net
cd santa-circles-api
tmux

# Link to default location of .well-known so letsencrypt works
ln -s ../santacirclesapi.artificialworlds.net/.well-known

# Create config file
echo 'export EMAIL_RELAY="smtp.example.com"' > config.env
echo 'export EMAIL_USERNAME="myusername"' >> config.env
echo 'export EMAIL_PASSWORD="mypassword"' >> config.env
echo 'export EMAIL_FROM_ADDRESS="me@example.com"' >> config.env
# Optional:
echo 'export EMAIL_CC_ADDRESS="admin@example.com"' >> config.env

mv santa-circles-api.new santa-circles-api

source config.env
./santa-circles-api
```

Proxy: santacirclesapi.artificialworlds.net 8088
