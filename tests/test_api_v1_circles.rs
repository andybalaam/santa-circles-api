extern crate santa_circles_api;

mod utils;
mod utils_circles;

use actix_web::web::Bytes;
use serde_json::json;

use utils::log_in_as::LogInAs;
use utils::new_app::{new_app, new_app_counting_saves, FakeSaver};
use utils::requests::{delete, get, get_nou, post, post_nou, put, test_req};
use utils_circles::json::{assert_matches, parse};

const USRA: LogInAs = LogInAs { u: "auser", p: "p" };
const ALICE: LogInAs = LogInAs { u: "ALICE", p: "a" };
const BOB: LogInAs = LogInAs { u: "BOB", p: "b" };
const CASSY: LogInAs = LogInAs { u: "CASSY", p: "c" };

impl LogInAs {
    pub fn un(&self) -> serde_json::Value {
        json!({ "username": self.u, "nickname": "" })
    }

    pub fn mem(&self) -> serde_json::Value {
        json!(
            {
                "username": self.u,
                "nickname": "" ,
                "wishlist_text": "",
                "wishlist_links": [],
            }
        )
    }
}

mod json_blobs {
    use super::{ModifiableJsonObject, USRA};
    use serde_json::{json, Value};

    pub fn new_circle_with_admins() -> Value {
        json! ({
            "name": "populated",
            "draw_date": "3rd Jan",
            "rules": "No running",
            "members": [
                {
                    "username": "a",
                    "nickname": "A A",
                    "wishlist_text": "",
                    "wishlist_links": [],
                },
                {
                    "username": "b",
                    "nickname": "b b",
                    "wishlist_text": "",
                    "wishlist_links": [],
                },
                {
                    "username": "c",
                    "nickname": "C c",
                    "wishlist_text": "",
                    "wishlist_links": [],
                },
            ],
            "organisers": [
                { "username":USRA.u, "nickname":"" },
                { "username": "a", "nickname": "A A"},
                { "username": "c", "nickname": "C c"},
                { "username": "d", "nickname": "DDD"},
            ],
            "draw": [],
            "disallowed_draw": [],
        })
    }

    pub fn new_circle_minimal() -> Value {
        json!({
            "name": "mycircle",
            "draw_date": "3rd Jan",
            "rules": "No running",
            "members": [],
            "organisers": [{"username":USRA.u,"nickname":""}],
            "draw": [],
            "disallowed_draw": [],
        })
    }

    pub fn new_circle_4members() -> Value {
        json!({
            "name": "mycircle",
            "draw_date": "3nd Feb, 2024",
            "rules": "Everything under \\u00A35",
            "members": [
                {
                    "username":"a",
                    "nickname":"A",
                    "wishlist_text": "",
                    "wishlist_links": [],
                },
                {
                    "username":"b",
                    "nickname":"B",
                    "wishlist_text": "",
                    "wishlist_links": [],
                },
                {
                    "username":"c",
                    "nickname":"C",
                    "wishlist_text": "",
                    "wishlist_links": [],
                },
                {
                    "username":"d",
                    "nickname":"D",
                    "wishlist_text": "",
                    "wishlist_links": [],
                },
            ],
            "organisers": [{"username":USRA.u,"nickname":""}],
            "draw": [],
            "disallowed_draw": [],
        })
    }

    pub fn new_circle_bad_members() -> Value {
        json!({
            "name": "Circle with bad members",
            "draw_date": "",
            "rules": "",
            "members": [
                {
                    "username":"",
                    "nickname":"B",
                    "wishlist_text": "",
                    "wishlist_links": [],
                },
                {
                    "username":"",
                    "nickname":"C",
                    "wishlist_text": "",
                    "wishlist_links": [],
                },
            ],
            "organisers": [{"username":USRA.u,"nickname":""}],
            "draw": [],
            "disallowed_draw": [],
        })
    }

    pub fn new_circle_with_restrictions() -> Value {
        json!({
            "name": "Circle 1",
            "draw_date": "3nd Feb, 2024",
            "rules": "Everything under a fiver",
            "members": [
                {
                    "username": "a",
                    "nickname": "A",
                    "wishlist_text": "",
                    "wishlist_links": [],
                },
                {
                    "username": "b",
                    "nickname": "B",
                    "wishlist_text": "",
                    "wishlist_links": [],
                },
                {
                    "username": "c",
                    "nickname": "C",
                    "wishlist_text": "",
                    "wishlist_links": [],
                },
            ],
            "organisers": [{"username":USRA.u, "nickname":""}],
            "draw": [],
            "disallowed_draw": [
                {"from_username": "a", "to_username": "b"}
            ],
        })
    }

    pub fn remove_draw(circle_json: Value) -> Value {
        let mut ret = circle_json.clone();
        ret.remove("draw");
        ret.remove("disallowed_draw");
        ret.insert("circleid", serde_json::Value::String(String::from("id1")));
        ret
    }
}

trait ModifiableJsonObject {
    fn remove(&mut self, name: &str);
    fn insert(&mut self, name: &str, value: serde_json::Value);
}

impl ModifiableJsonObject for serde_json::Value {
    fn remove(&mut self, name: &str) {
        self.as_object_mut().unwrap().remove(name);
    }

    fn insert(&mut self, name: &str, value: serde_json::Value) {
        self.as_object_mut()
            .unwrap()
            .insert(String::from(name), value);
    }
}

#[test]
fn new_user_has_no_circles() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());

    let (status, resp) = get(&mut app, USRA, "/v1/circle");
    assert_eq!(status, 200);
    assert_matches(
        &parse(&resp),
        json!(
            {
                "username": "auser",
                "member_circles": [],
                "organiser_circles": [],
            }
        ),
    );
}

#[test]
fn looking_at_my_circles_with_incorrect_password_is_forbidden() {
    let mut app = new_app();
    test_req(
        post_nou(&mut app, "/v1/setup", &USRA.creation_json()),
        r#"201 {"username":"auser","admin":true}"#,
    );
    let wrong_password = LogInAs {
        u: "auser",
        p: "WRONG",
    };

    test_req(
        get(&mut app, wrong_password, "/v1/circle"),
        r#"401 Invalid username or password"#,
    );
}

#[test]
fn looking_at_my_circles_with_no_auth_header_is_forbidden() {
    let mut app = new_app();
    test_req(
        post_nou(&mut app, "/v1/setup", &USRA.creation_json()),
        r#"201 {"username":"auser","admin":true}"#,
    );

    // Don't supply a username or password at all - should fail
    test_req(get_nou(&mut app, "/v1/circle"), r#"401 "#);
}

#[test]
fn a_newly_added_circle_is_shown() {
    let saver = FakeSaver::new();
    let mut app = new_app_counting_saves(saver.clone());
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(
            &mut app,
            USRA,
            "/v1/circle",
            &json_blobs::new_circle_minimal().to_string(),
        ),
        r#"201 {"circleid":"id1","name":"mycircle"}"#,
    );
    assert_eq!(saver.num_saves(), 3);
    let (status, resp) = get(&mut app, USRA, "/v1/circle");
    assert_eq!(status, 200);
    assert_matches(
        &parse(&resp),
        json!(
            {
                "username": "auser",
                "member_circles": [],
                "organiser_circles": [{"circleid":"id1", "name":"mycircle"}],
            }
        ),
    );
    assert_eq!(saver.num_saves(), 4);
}

#[test]
fn creating_a_circle_i_am_not_organising_is_an_error() {
    let mut circle_json = json_blobs::new_circle_minimal();
    circle_json.insert(
        "members",
        json!([
            {
                "username":USRA.u,
                "nickname":"",
                "wishlist_text": "",
                "wishlist_links": [],
            }
        ]),
    );
    circle_json
        .insert("organisers", json!([{"username":"other", "nickname":""}]));

    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle_json.to_string()),
        r#"400 {"error":"The user creating a circle must be an organiser."}"#,
    );
}

#[test]
fn removing_the_last_organiser_is_not_allowed() {
    let mut circle_json = json_blobs::new_circle_minimal();
    circle_json.insert("organisers", json!([USRA.un(), ALICE.un()]));

    // Given a circle with 2 organisers
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/user", &ALICE.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle_json.to_string()),
        r#"201 {"circleid":"id1","name":"mycircle"}"#,
    );

    // I can remove 1
    circle_json.insert("organisers", json!([ALICE.un()]));
    test_req(
        put(&mut app, USRA, "/v1/circle/id1", &circle_json.to_string()),
        r#"200 {"circleid":"id1","name":"mycircle"}"#,
    );

    // But not the last one
    circle_json.insert("organisers", json!([]));
    test_req(
        put(&mut app, ALICE, "/v1/circle/id1", &circle_json.to_string()),
        r#"400 {"error":"There must be at least one organiser for a circle."}"#,
    );
}

#[test]
fn a_newly_added_circle_can_be_retrieved() {
    let circle_json = json_blobs::new_circle_with_admins();
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle_json.to_string()),
        r#"201 {"circleid":"id1","name":"populated"}"#,
    );

    let mut expected_json = json_blobs::remove_draw(circle_json);
    expected_json.insert("recipients", json!(null));
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1");
    assert_eq!(status, 200);
    assert_eq!(&parse(&resp).to_string(), &expected_json.to_string());
}

#[test]
fn a_circle_with_empty_name_can_be_retrieved() {
    let mut circle_json = json_blobs::new_circle_minimal();
    circle_json.insert("name", serde_json::Value::String(String::from("")));

    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle_json.to_string()),
        r#"201 {"circleid":"id1","name":""}"#,
    );
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), json_blobs::remove_draw(circle_json));
}

#[test]
fn a_circle_can_be_updated() {
    let mut old_circle = json_blobs::new_circle_minimal();
    old_circle.insert("name", serde_json::Value::String(String::from("old")));

    let mut new_circle = json_blobs::new_circle_with_admins();
    new_circle.insert("name", serde_json::Value::String(String::from("new")));

    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &old_circle.to_string()),
        r#"201 {"circleid":"id1","name":"old"}"#,
    );
    test_req(
        put(&mut app, USRA, "/v1/circle/id1", &new_circle.to_string()),
        r#"200 {"circleid":"id1","name":"new"}"#,
    );
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), json_blobs::remove_draw(new_circle));
}

#[test]
fn can_update_a_circle_with_member_wishlists() {
    let mut old_circle = json_blobs::new_circle_minimal();
    old_circle.insert("name", serde_json::Value::String(String::from("old")));

    let mut new_circle = json_blobs::new_circle_with_admins();
    new_circle.insert("name", serde_json::Value::String(String::from("new")));
    new_circle.insert(
        "members",
        json! {[
            {
                "username": "user_a",
                "nickname": "UserA",
                "wishlist_text": "My wish list.",
                "wishlist_links": [
                    { "text": "Link 1", "url": "https://link1.example.com" },
                    { "text": "Link 2", "url": "https://link2.example.com" },
                ],
            }
        ]},
    );

    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &old_circle.to_string()),
        r#"201 {"circleid":"id1","name":"old"}"#,
    );
    test_req(
        put(&mut app, USRA, "/v1/circle/id1", &new_circle.to_string()),
        r#"200 {"circleid":"id1","name":"new"}"#,
    );
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), json_blobs::remove_draw(new_circle));
}

#[test]
fn updating_members_without_including_wishlist_preserves_wishlist() {
    let mut old_circle = json_blobs::new_circle_minimal();
    old_circle.insert("name", serde_json::Value::String(String::from("old")));
    old_circle.insert(
        "members",
        json! {[
            {
                "username": "user_a",
                "nickname": "User A",
                "wishlist_text": "My wish list.",
                "wishlist_links": [
                    { "text": "Link 1", "url": "https://link1.example.com" },
                    { "text": "Link 2", "url": "https://link2.example.com" },
                ],
            },
            {
                "username": "user_b",
                "nickname": "User B",
                "wishlist_text": "B's wish list.",
                "wishlist_links": [
                    { "text": "Link B1", "url": "https://linkb1.example.com" },
                    { "text": "Link B2", "url": "https://linkb2.example.com" },
                ],
            }
        ]},
    );

    let mut new_circle = json_blobs::new_circle_minimal();
    new_circle.insert("name", serde_json::Value::String(String::from("new")));
    new_circle.insert(
        "members",
        json! {[
            {
                "username": "user_a",
                "nickname": "User A renamed", // Rename A
            },
            // Delete B
            {
                "username": "user_c",
                "nickname": "User C", // Add C
            }
        ]},
    );

    let mut expected = json_blobs::new_circle_minimal();
    expected.insert("name", serde_json::Value::String(String::from("new")));
    expected.insert(
        "members",
        json! {[
            {
                "username": "user_a",
                "nickname": "User A renamed",
                "wishlist_text": "My wish list.",
                "wishlist_links": [
                    { "text": "Link 1", "url": "https://link1.example.com" },
                    { "text": "Link 2", "url": "https://link2.example.com" },
                ],
            },
            {
                "username": "user_c",
                "nickname": "User C",
                "wishlist_text": "",
                "wishlist_links": [],
            }
        ]},
    );

    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &old_circle.to_string()),
        r#"201 {"circleid":"id1","name":"old"}"#,
    );
    test_req(
        put(&mut app, USRA, "/v1/circle/id1", &new_circle.to_string()),
        r#"200 {"circleid":"id1","name":"new"}"#,
    );
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), json_blobs::remove_draw(expected));
}

#[test]
fn multiple_circles_can_exist() {
    let mut circle1 = json_blobs::new_circle_minimal();
    circle1.insert("name", serde_json::Value::String(String::from("Circle 1")));
    circle1.insert(
        "members",
        json!([
            {
                "username":"buser",
                "nickname":"",
                "wishlist_text": "",
                "wishlist_links": [],
            }
        ]),
    );

    let mut circle2 = json_blobs::new_circle_minimal();
    circle2.insert("name", serde_json::Value::String(String::from("Circle 2")));
    circle2.insert(
        "members",
        json!([
            {
                "username":"auser",
                "nickname":"",
                "wishlist_text": "",
                "wishlist_links": [],
            }
        ]),
    );

    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle1.to_string()),
        r#"201 {"circleid":"id1","name":"Circle 1"}"#,
    );
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle2.to_string()),
        r#"201 {"circleid":"id2","name":"Circle 2"}"#,
    );
    let (status, resp) = get(&mut app, USRA, "/v1/circle");
    assert_eq!(status, 200);
    assert_matches(
        &parse(&resp),
        json!(
            {
               "username": "auser",
                "member_circles": [
                    {"circleid":"id2","name":"Circle 2"},
                ],
                "organiser_circles": [
                    {"circleid":"id1","name":"Circle 1"},
                    {"circleid":"id2","name":"Circle 2"},
                ],
            }
        ),
    );
}

#[test]
fn fetching_a_nonexistent_circle_is_an_error() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(
            &mut app,
            USRA,
            "/v1/circle",
            &json_blobs::new_circle_minimal().to_string(),
        ),
        r#"201 {"circleid":"id1","name":"mycircle"}"#,
    );
    test_req(
        get(&mut app, USRA, "/v1/circle/nonexistentcircle"),
        r#"404 {"error":"Circle does not exist."}"#,
    );
}

#[test]
fn users_cannot_see_or_modify_each_others_circles() {
    let mut circle1 = json_blobs::new_circle_minimal();
    circle1.insert("name", serde_json::Value::String(String::from("Circle 1")));
    circle1.insert("organisers", json!([{"username":"ALICE", "nickname":""}]));

    let mut circle2 = json_blobs::new_circle_minimal();
    circle2.insert("name", serde_json::Value::String(String::from("Circle 2")));
    circle2.insert("organisers", json!([{"username":"ALICE", "nickname":""}]));

    let mut updated1 = json_blobs::new_circle_minimal();
    updated1.insert("name", serde_json::Value::String(String::from("Updated")));
    updated1.insert("organisers", json!([{"username":"ALICE", "nickname":""}]));

    // Given some users exist
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());

    post(&mut app, USRA, "/v1/user", &ALICE.creation_json());
    post(&mut app, USRA, "/v1/user", &BOB.creation_json());

    // and alice has some circles
    test_req(
        post(&mut app, ALICE, "/v1/circle", &circle1.to_string()),
        r#"201 {"circleid":"id1","name":"Circle 1"}"#,
    );
    test_req(
        post(&mut app, ALICE, "/v1/circle", &circle2.to_string()),
        r#"201 {"circleid":"id2","name":"Circle 2"}"#,
    );

    // Then bob cannot list them
    test_req(
        get(&mut app, BOB, "/v1/circle"),
        r#"200 {"username":"BOB","member_circles":[],"organiser_circles":[]}"#,
    );
    // Or get them
    test_req(
        get(&mut app, BOB, "/v1/circle/id1"),
        r#"404 {"error":"Circle does not exist."}"#,
    );
    // Or update them
    test_req(
        put(&mut app, BOB, "/v1/circle/id1", &updated1.to_string()),
        r#"404 {"error":"Circle does not exist."}"#,
    );
}

#[test]
fn members_cannot_update_the_circle() {
    let mut circle = json_blobs::new_circle_minimal();
    circle.insert("name", serde_json::Value::String(String::from("Circle 1")));
    circle.insert(
        "members",
        json!([
            {
                "username":"BOB",
                "nickname":"Bob",
                "wishlist_text": "",
                "wishlist_links": [],
            }
        ]),
    );
    circle.insert("organisers", json!([{"username":"ALICE", "nickname":""}]));

    let mut updated = json_blobs::new_circle_minimal();
    updated.insert("name", serde_json::Value::String(String::from("Updated")));
    updated.insert("organisers", json!([{"username":"ALICE", "nickname":""}]));

    // Given some users exist
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/user", &ALICE.creation_json());
    post(&mut app, USRA, "/v1/user", &BOB.creation_json());

    // and alice is organising a circle with bob in
    test_req(
        post(&mut app, ALICE, "/v1/circle", &circle.to_string()),
        r#"201 {"circleid":"id1","name":"Circle 1"}"#,
    );

    // Then bob can list it
    let (status, resp) = get(&mut app, BOB, "/v1/circle");
    assert_eq!(status, 200);
    assert_matches(
        &parse(&resp),
        json!(
            {
               "username": "BOB",
                "member_circles": [
                    {"circleid":"id1","name":"Circle 1"},
                ],
                "organiser_circles": [],
            }
        ),
    );
    // and get it
    let (status, resp) = get(&mut app, BOB, "/v1/circle/id1");
    assert_eq!(status, 200);
    assert_matches(
        &parse(&resp),
        json!({
            "name": "Circle 1",
            "members": [
            {
                "username":"BOB",
                "nickname":"Bob",
                "wishlist_text": "",
                "wishlist_links": [],
            }
            ],
            "organisers": [{"username":"ALICE", "nickname":""}],
        }),
    );
    // But can't update it
    test_req(
        put(&mut app, BOB, "/v1/circle/id1", &updated.to_string()),
        r#"403 {"error":"Permission denied."}"#,
    );
}

#[test]
fn members_cannot_view_circle_in_organise_mode() {
    let mut circle = json_blobs::new_circle_minimal();
    circle.insert("name", serde_json::Value::String(String::from("Circle 1")));
    circle.insert(
        "members",
        json!([
            {
                "username":"BOB",
                "nickname":"Bob",
                "wishlist_text": "",
                "wishlist_links": [],
            }
        ]),
    );
    circle.insert("organisers", json!([{"username":"ALICE", "nickname":""}]));

    // Given some users exist
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/user", &ALICE.creation_json());
    post(&mut app, USRA, "/v1/user", &BOB.creation_json());

    // and alice is organising a circle with bob in
    test_req(
        post(&mut app, ALICE, "/v1/circle", &circle.to_string()),
        r#"201 {"circleid":"id1","name":"Circle 1"}"#,
    );

    // Then bob can get it
    let (status, _resp) = get(&mut app, BOB, "/v1/circle/id1");
    assert_eq!(status, 200);
    // But can't get it with view=organise
    test_req(
        get(&mut app, BOB, "/v1/circle/id1?view=organise"),
        r#"403 {"error":"Permission denied."}"#,
    );

    // And alice can see the organiser mode (containing draw etc.)
    let (status, resp) = get(&mut app, ALICE, "/v1/circle/id1?view=organise");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), circle);
}

#[test]
fn members_can_see_the_wishlists_of_their_recipients() {
    let members = json!([
        {
            "username":"ALICE",
            "nickname":"",
            "wishlist_text": "",
            "wishlist_links": [],
        },
        {
            "username":"BOB",
            "nickname":"Bob",
            "wishlist_text": "B wish",
            "wishlist_links": [
                { "text": "B w1", "url":"http://b.com" },
                { "text": "B w2", "url":"http://b.com" },
            ]
        },
        {
            "username":"CASSY",
            "nickname":"Cassy",
            "wishlist_text": "C wish",
            "wishlist_links": [
                { "text": "C w1", "url":"http://c.com" },
                { "text": "C w2", "url":"http://c.com" },
            ]
        },
    ]);
    let mut circle = json_blobs::new_circle_minimal();
    circle.insert("members", members.clone());
    circle.insert(
        "draw",
        json!([
            { "from_username": "ALICE", "to_username": "BOB" },
            { "from_username": "ALICE", "to_username": "CASSY" },
        ]),
    );
    let mut expected = json_blobs::new_circle_minimal();
    expected.insert("members", members);
    expected.insert("recipients", json! {["BOB", "CASSY"]});

    // Given some users exist
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/user", &ALICE.creation_json());
    post(&mut app, USRA, "/v1/user", &BOB.creation_json());
    post(&mut app, USRA, "/v1/user", &CASSY.creation_json());

    // and User A is organising the circle
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle.to_string()),
        r#"201 {"circleid":"id1","name":"mycircle"}"#,
    );

    // Then Alice can get it, and see everyone she is buying for
    let (status, resp) = get(&mut app, ALICE, "/v1/circle/id1");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), json_blobs::remove_draw(expected));
}

#[test]
fn members_can_update_their_own_wishlists() {
    let mut circle = json_blobs::new_circle_minimal();
    circle.insert(
        "members",
        json!([
            {
                "username":"ALICE",
                "nickname":"",
                "wishlist_text": "",
                "wishlist_links": [],
            },
            {
                "username":"BOB",
                "nickname":"Bob",
                "wishlist_text": "",
                "wishlist_links": []
            },
        ]),
    );
    let mut expected = json_blobs::new_circle_minimal();
    expected.insert(
        "members",
        json!([
            {
                "username":"ALICE",
                "nickname":"",
                "wishlist_text": "My wishes",
                "wishlist_links": [
                    { "text": "link 1", "url": "http://l1.e.com"},
                ],
            },
            {
                "username":"BOB",
                "nickname":"Bob",
                "wishlist_text": "",
                "wishlist_links": []
            },
        ]),
    );

    // Given Alice is a member of the circle
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/user", &ALICE.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle.to_string()),
        r#"201 {"circleid":"id1","name":"mycircle"}"#,
    );

    // When Alice updates her wishlist
    test_req(
        put(
            &mut app,
            ALICE,
            "/v1/circle/id1/members/ALICE/wishlist",
            &json!({
                "wishlist_text": "My wishes",
                "wishlist_links": [
                    { "text": "link 1", "url": "http://l1.e.com"},
                ],
            })
            .to_string(),
        ),
        r#"200 {"circleid":"id1","username":"ALICE"}"#,
    );

    // Then the change is reflected
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), json_blobs::remove_draw(expected));
}

#[test]
fn members_cannot_update_other_members_wishlists() {
    let mut circle = json_blobs::new_circle_minimal();
    circle.insert(
        "members",
        json!([
            {
                "username":"ALICE",
                "nickname":"",
                "wishlist_text": "",
                "wishlist_links": [],
            },
            {
                "username":"BOB",
                "nickname":"Bob",
                "wishlist_text": "",
                "wishlist_links": []
            },
        ]),
    );

    // Given Alice is a member of the circle
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/user", &ALICE.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle.to_string()),
        r#"201 {"circleid":"id1","name":"mycircle"}"#,
    );

    // When Alice attempts to update Bob's wishlist
    test_req(
        put(
            &mut app,
            ALICE,
            "/v1/circle/id1/members/BOB/wishlist",
            &json!({
                "wishlist_text": "My wishes",
                "wishlist_links": [
                    { "text": "link 1", "url": "http://l1.e.com"},
                ],
            })
            .to_string(),
        ),
        r#"403 {"error":"Permission denied."}"#,
    );

    // Then the circle is unchanged
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), json_blobs::remove_draw(circle));
}

#[test]
fn organisers_can_update_the_draw() {
    let mut circle = json_blobs::new_circle_minimal();
    circle.insert("name", serde_json::Value::String(String::from("Circle 1")));
    circle.insert(
        "members",
        json!([
            {
                "username":"BOB",
                "nickname":"Bob",
                "wishlist_text": "",
                "wishlist_links": [],
            }
        ]),
    );
    circle.insert("organisers", json!([{"username":"ALICE", "nickname":""}]));

    let mut updated = json_blobs::new_circle_minimal();
    updated.insert("name", serde_json::Value::String(String::from("Updated")));
    updated.insert(
        "members",
        json!([
            {
                "username": "a",
                "nickname": "A",
                "wishlist_text": "",
                "wishlist_links": [],
            },
            {
                "username": "b",
                "nickname": "B",
                "wishlist_text": "",
                "wishlist_links": [],
            },
        ]),
    );
    updated.insert("organisers", json!([{"username":"ALICE", "nickname":""}]));
    updated.insert(
        "draw",
        json!( [
            {"from_username": "a", "to_username": "b"},
        ]),
    );

    // Given some users exist
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/user", &ALICE.creation_json());
    post(&mut app, USRA, "/v1/user", &BOB.creation_json());

    // and alice is organising a circle with bob in
    test_req(
        post(&mut app, ALICE, "/v1/circle", &circle.to_string()),
        r#"201 {"circleid":"id1","name":"Circle 1"}"#,
    );

    // When alice updates the draw
    test_req(
        put(&mut app, ALICE, "/v1/circle/id1", &updated.to_string()),
        r#"200 {"circleid":"id1","name":"Updated"}"#,
    );

    // Then the updates stick
    let (status, resp) = get(&mut app, ALICE, "/v1/circle/id1?view=organise");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), updated);
}

#[test]
fn organisers_can_request_an_automatic_draw() {
    let before = json_blobs::new_circle_4members();
    let mut request = json_blobs::new_circle_4members();
    request.insert("name", json!("Updated"));
    request.insert("request_draw", json!(true));
    request.insert("deterministic_draw", json!(true));
    let mut drawn = json_blobs::new_circle_4members();
    drawn.insert("name", json!("Updated"));
    drawn.insert(
        "draw",
        json!([
              // Draw is in alphabetical order of from_username
            {"from_username": "a", "to_username": "d"},
            {"from_username": "b", "to_username": "a"},
            {"from_username": "c", "to_username": "b"},
            {"from_username": "d", "to_username": "c"},
        ]),
    );

    // Given we are organising a circle
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &before.to_string()),
        r#"201 {"circleid":"id1","name":"mycircle"}"#,
    );

    // When we update it with a draw_request
    test_req(
        put(&mut app, USRA, "/v1/circle/id1", &request.to_string()),
        r#"200 {"circleid":"id1","name":"Updated"}"#,
    );

    // Then a draw has been done
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1?view=organise");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), drawn);
}

#[test]
fn invalid_draw_contains_warnings() {
    fn noone_gives_to_a(circle: &mut serde_json::Value) {
        circle.insert(
            "disallowed_draw",
            json!([
                {"from_username": "b", "to_username": "a"},
                {"from_username": "d", "to_username": "a"},
                {"from_username": "c", "to_username": "a"},
            ]),
        );
    }

    let mut before = json_blobs::new_circle_4members();
    let mut request = json_blobs::new_circle_4members();
    request.insert("name", json!("Updated"));
    request.insert("request_draw", json!(true));
    request.insert("deterministic_draw", json!(true));
    let mut drawn = json_blobs::new_circle_4members();
    drawn.insert("name", json!("Updated"));
    drawn.insert(
        "draw",
        json!([
            {"from_username": "a", "to_username": "b"},
            {"from_username": "b", "to_username": "c"},
            {"from_username": "c", "to_username": "d"},
            {"from_username": "d", "to_username": "a"},
        ]),
    );
    drawn.insert(
        "draw_warnings",
        json!([
              {"BreaksRestriction":{"from_username":"d","to_username":"a"}},
        ]),
    );

    noone_gives_to_a(&mut before);
    noone_gives_to_a(&mut request);
    noone_gives_to_a(&mut drawn);

    // Given we are organising a circle
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &before.to_string()),
        r#"201 {"circleid":"id1","name":"mycircle"}"#,
    );

    // When we update it with a draw_request that cannot be satisified
    test_req(
        put(&mut app, USRA, "/v1/circle/id1", &request.to_string()),
        r#"200 {"circleid":"id1","name":"Updated"}"#,
    );

    // Then a draw has been done but with warnings
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1?view=organise");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), drawn);
}

#[test]
fn can_request_draw_on_creation() {
    fn noone_gives_to_a(circle: &mut serde_json::Value) {
        circle.insert(
            "disallowed_draw",
            json!([
                {"from_username": "b", "to_username": "a"},
                {"from_username": "d", "to_username": "a"},
                {"from_username": "c", "to_username": "a"},
            ]),
        );
    }

    let mut request = json_blobs::new_circle_4members();
    request.insert("request_draw", json!(true));
    request.insert("deterministic_draw", json!(true));
    let mut drawn = json_blobs::new_circle_4members();
    drawn.insert(
        "draw",
        json!([
            {"from_username": "a", "to_username": "b"},
            {"from_username": "b", "to_username": "c"},
            {"from_username": "c", "to_username": "d"},
            {"from_username": "d", "to_username": "a"},
        ]),
    );
    drawn.insert(
        "draw_warnings",
        json!([
              {"BreaksRestriction":{"from_username":"d","to_username":"a"}},
        ]),
    );

    noone_gives_to_a(&mut request);
    noone_gives_to_a(&mut drawn);

    // When we create a circle, asking for an impossible draw immediately
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &request.to_string()),
        r#"201 {"circleid":"id1","name":"mycircle"}"#,
    );

    // Then a draw has been done but with warnings
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1?view=organise");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), drawn);
}

#[test]
fn creating_with_invalid_draw_shows_warnings() {
    fn bad_draw(circle: &mut serde_json::Value) {
        circle.insert(
            "draw",
            json!([
                {"from_username": "b", "to_username": "a"},
                {"from_username": "c", "to_username": "a"},
            ]),
        );
    }

    let mut circle = json_blobs::new_circle_4members();
    let mut expected = json_blobs::new_circle_4members();
    expected.insert(
        "draw_warnings",
        json!([
            {"NotGiving":"a"},
            {"ReceivingMultiple":["a",["c","b"]]},
            {"NotReceiving":"b"},
            {"NotReceiving":"c"},
            {"NotGiving":"d"},
            {"NotReceiving":"d"},
        ]),
    );

    bad_draw(&mut circle);
    bad_draw(&mut expected);

    // When we create a circle, including a bad draw
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle.to_string()),
        r#"201 {"circleid":"id1","name":"mycircle"}"#,
    );

    // Then the warnings were added
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1?view=organise");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), expected);
}

#[test]
fn updating_with_invalid_draw_shows_warnings() {
    fn bad_draw(circle: &mut serde_json::Value) {
        circle.insert(
            "draw",
            json!([
                {"from_username": "a", "to_username": "b"},
                {"from_username": "a", "to_username": "c"},
            ]),
        );
    }

    let circle = json_blobs::new_circle_4members();
    let mut update = json_blobs::new_circle_4members();
    let mut expected = json_blobs::new_circle_4members();
    expected.insert(
        "draw_warnings",
        json!([
            {"GivingMultiple":["a",["c","b"]]},
            {"NotReceiving":"a"},
            {"NotGiving":"b"},
            {"NotGiving":"c"},
            {"NotGiving":"d"},
            {"NotReceiving":"d"},
        ]),
    );

    bad_draw(&mut update);
    bad_draw(&mut expected);

    // Given we have a circle
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle.to_string()),
        r#"201 {"circleid":"id1","name":"mycircle"}"#,
    );

    // When we update it with a bad draw
    test_req(
        put(&mut app, USRA, "/v1/circle/id1", &update.to_string()),
        r#"200 {"circleid":"id1","name":"mycircle"}"#,
    );

    // Then the warnings were added
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1?view=organise");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), expected);
}

#[test]
fn member_without_username_is_a_warning() {
    let circle = json_blobs::new_circle_bad_members();
    let mut expected = json_blobs::new_circle_bad_members();
    expected.insert(
        "draw_warnings",
        json!([
            {"NoUsername":"B"},
            {"NoUsername":"C"},
            {"NotGiving":""},
            {"NotReceiving":""},
            {"NotGiving":""},
            {"NotReceiving":""},
        ]),
    );

    // Given we have a circle
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle.to_string()),
        r#"201 {"circleid":"id1","name":"Circle with bad members"}"#,
    );

    // Then the warnings were added
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1?view=organise");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), expected);
}

#[test]
fn the_draw_is_different_every_time() {
    let before = json_blobs::new_circle_4members();
    let mut request = json_blobs::new_circle_4members();
    request.insert("name", json!("Updated"));
    request.insert("request_draw", json!(true));

    // Given we are organising a circle
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &before.to_string()),
        r#"201 {"circleid":"id1","name":"mycircle"}"#,
    );

    test_req(
        put(&mut app, USRA, "/v1/circle/id1", &request.to_string()),
        r#"200 {"circleid":"id1","name":"Updated"}"#,
    );

    // Make a draw
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1?view=organise");
    assert_eq!(status, 200);
    let mut prev_draw = parse(&resp).get("draw").unwrap().to_string();

    // Keep on;making draws until one is different
    for _ in 0..20 {
        test_req(
            put(&mut app, USRA, "/v1/circle/id1", &request.to_string()),
            r#"200 {"circleid":"id1","name":"Updated"}"#,
        );

        let (s, resp) = get(&mut app, USRA, "/v1/circle/id1?view=organise");
        assert_eq!(s, 200);
        let draw = parse(&resp).get("draw").unwrap().to_string(); // Each time we make a draw request
        if draw != prev_draw {
            // We found a different draw - success
            return;
        }
        prev_draw = draw;
        // The previous draw was the same as this one - try again
    }

    panic!("All draws were the same!");
}

#[test]
fn organisers_can_update_restrictions() {
    // Given a circle
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(
            &mut app,
            USRA,
            "/v1/circle",
            &json_blobs::new_circle_minimal().to_string(),
        ),
        r#"201 {"circleid":"id1","name":"mycircle"}"#,
    );

    // When we update the restrictions
    test_req(
        put(
            &mut app,
            USRA,
            "/v1/circle/id1",
            &json_blobs::new_circle_with_restrictions().to_string(),
        ),
        r#"200 {"circleid":"id1","name":"Circle 1"}"#,
    );

    // Then the updates stick
    let (status, resp) = get(&mut app, USRA, "/v1/circle/id1?view=organise");
    assert_eq!(status, 200);
    assert_matches(&parse(&resp), json_blobs::new_circle_with_restrictions());
}

#[test]
fn circles_can_be_deleted() {
    let mut circle1 = json_blobs::new_circle_minimal();
    circle1.insert("name", serde_json::Value::String(String::from("Circle 1")));

    let mut circle2 = json_blobs::new_circle_minimal();
    circle2.insert("name", serde_json::Value::String(String::from("Circle 2")));

    let mut circle3 = json_blobs::new_circle_minimal();
    circle3.insert("name", serde_json::Value::String(String::from("Circle 3")));

    // Given a user with circles
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle1.to_string()),
        r#"201 {"circleid":"id1","name":"Circle 1"}"#,
    );
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle2.to_string()),
        r#"201 {"circleid":"id2","name":"Circle 2"}"#,
    );
    test_req(
        post(&mut app, USRA, "/v1/circle", &circle3.to_string()),
        r#"201 {"circleid":"id3","name":"Circle 3"}"#,
    );

    // When I delete a circle
    test_req(delete(&mut app, USRA, "/v1/circle/id2"), r#"204 "#);

    // It is gone
    let (status, resp) = get(&mut app, USRA, "/v1/circle");
    assert_eq!(status, 200);
    assert_matches(
        &parse(&resp),
        json!(
            {
               "username": "auser",
                "member_circles": [],
                "organiser_circles": [
                    {"circleid":"id1", "name": "Circle 1"},
                    {"circleid":"id3", "name": "Circle 3"},
                ],
            }
        ),
    );
}

#[test]
fn new_users_can_see_their_circles() {
    // Given a circle with someone as yet unknown as a member and organiser
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());

    // (This user is not logged in here)
    let later_user = LogInAs { u: "l", p: "l" };

    test_req(
        post(
            &mut app,
            USRA,
            "/v1/circle",
            &json!( {
                "name": "Circle 1",
                "draw_date": "",
                "rules": "",
                "members": [later_user.mem()],
                "organisers": [USRA.un(), later_user.un()],
                "draw": [],
                "disallowed_draw": [],
            })
            .to_string(),
        ),
        r#"201 {"circleid":"id1","name":"Circle 1"}"#,
    );

    // When we create the user later
    post(&mut app, USRA, "/v1/user", &later_user.creation_json());

    // Then the user is able to see the circle (as member and organiser)
    let (status, resp) = get(&mut app, later_user, "/v1/circle");
    assert_eq!(status, 200);
    assert_matches(
        &parse(&resp),
        json!( {
            "username": "l",
            "member_circles": [{"circleid": "id1", "name": "Circle 1"}],
            "organiser_circles": [{"circleid": "id1", "name": "Circle 1"}]
        }),
    );
}

#[test]
fn membership_settings_affect_members_own_visibility() {
    // Given a circle with various members and organisers

    let mem_onl = LogInAs { u: "m", p: "m" };
    let org_onl = LogInAs { u: "o", p: "o" };
    let mem_org = LogInAs { u: "mo", p: "mo" };

    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/user", &mem_onl.creation_json());
    post(&mut app, USRA, "/v1/user", &org_onl.creation_json());
    post(&mut app, USRA, "/v1/user", &mem_org.creation_json());

    test_req(
        post(
            &mut app,
            USRA,
            "/v1/circle",
            &json!( {
                "name": "Circle 1",
                "draw_date": "",
                "rules": "",
                "members": [mem_onl.mem(), mem_org.mem()],
                "organisers": [USRA.un(), mem_org.un(), org_onl.un()],
                "draw": [],
                "disallowed_draw": [],
            })
            .to_string(),
        ),
        r#"201 {"circleid":"id1","name":"Circle 1"}"#,
    );

    let (status, resp) = get(&mut app, mem_onl, "/v1/circle");
    assert_eq!(status, 200);
    assert_matches(
        &parse(&resp),
        json!( {
            "username": "m",
            "member_circles": [{"circleid": "id1", "name": "Circle 1"}],
            "organiser_circles": []
        }),
    );

    let (status, resp) = get(&mut app, org_onl, "/v1/circle");
    assert_eq!(status, 200);
    assert_matches(
        &parse(&resp),
        json!( {
            "username": "o",
            "member_circles": [],
            "organiser_circles": [{"circleid": "id1", "name": "Circle 1"}]
        }),
    );

    let (status, resp) = get(&mut app, mem_org, "/v1/circle");
    assert_eq!(status, 200);
    assert_matches(
        &parse(&resp),
        json!({
            "username": "mo",
            "member_circles": [{"circleid": "id1", "name": "Circle 1"}],
            "organiser_circles": [{"circleid": "id1", "name": "Circle 1"}]
        }),
    );
}

#[test]
fn membership_updates_change_members_own_visibility() {
    // Given a circle with various members and organisers

    let mem_onl = LogInAs { u: "m", p: "m" };
    let org_onl = LogInAs { u: "o", p: "o" };
    let mem_org = LogInAs { u: "mo", p: "mo" };

    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/user", &mem_onl.creation_json());
    post(&mut app, USRA, "/v1/user", &org_onl.creation_json());
    post(&mut app, USRA, "/v1/user", &mem_org.creation_json());

    test_req(
        post(
            &mut app,
            USRA,
            "/v1/circle",
            &json!({
                "name": "Circle 1",
                "draw_date": "",
                "rules": "",
                "members": [org_onl.mem()],
                "organisers": [USRA.un(), mem_onl.un()],
                "draw": [],
                "disallowed_draw": [],
            })
            .to_string(),
        ),
        r#"201 {"circleid":"id1","name":"Circle 1"}"#,
    );

    // When I update it to have certain members and organisers
    test_req(
        put(
            &mut app,
            USRA,
            "/v1/circle/id1",
            &json!({
                "name": "Circle 1",
                "draw_date": "",
                "rules": "",
                "members": [mem_org.mem(), mem_onl.mem()],
                "organisers": [USRA.un(), mem_org.un(), org_onl.un()],
                "draw": [],
                "disallowed_draw": [],
            })
            .to_string(),
        ),
        r#"200 {"circleid":"id1","name":"Circle 1"}"#,
    );

    // Then those members see the correct listing of circles they are in
    let (status, resp) = get(&mut app, mem_onl, "/v1/circle");
    assert_eq!(status, 200);
    assert_matches(
        &parse(&resp),
        json!({
            "username": "m",
            "member_circles": [{"circleid": "id1", "name": "Circle 1"}],
            "organiser_circles": []
        }),
    );

    let (status, resp) = get(&mut app, org_onl, "/v1/circle");
    assert_eq!(status, 200);
    assert_matches(
        &parse(&resp),
        json!( {
            "username": "o",
            "member_circles": [],
            "organiser_circles": [{"circleid": "id1", "name": "Circle 1"}]
        }),
    );

    let (status, resp) = get(&mut app, mem_org, "/v1/circle");
    assert_eq!(status, 200);
    assert_matches(
        &parse(&resp),
        json!({
            "username": "mo",
            "member_circles": [{"circleid": "id1", "name": "Circle 1"}],
            "organiser_circles": [{"circleid": "id1", "name": "Circle 1"}]
        }),
    );
}

#[test]
fn deleting_circle_changes_members_own_visibility() {
    // Given 2 circles with various members and organisers

    let mem_onl = LogInAs { u: "m", p: "m" };
    let org_onl = LogInAs { u: "o", p: "o" };
    let mem_org = LogInAs { u: "mo", p: "mo" };

    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/user", &mem_onl.creation_json());
    post(&mut app, USRA, "/v1/user", &org_onl.creation_json());
    post(&mut app, USRA, "/v1/user", &mem_org.creation_json());

    test_req(
        post(
            &mut app,
            USRA,
            "/v1/circle",
            &json!( {
                "name": "Circle 1",
                "draw_date": "",
                "rules": "",
                "members": [mem_onl.mem(), mem_org.mem()],
                "organisers": [USRA.un(), mem_org.un(), org_onl.un()],
                "draw": [],
                "disallowed_draw": [],
            })
            .to_string(),
        ),
        r#"201 {"circleid":"id1","name":"Circle 1"}"#,
    );

    test_req(
        post(
            &mut app,
            USRA,
            "/v1/circle",
            &json!( {
                "name": "Circle 2",
                "draw_date": "",
                "rules": "",
                "members": [mem_onl.mem(), mem_org.mem()],
                "organisers": [USRA.un(), mem_org.un(), org_onl.un()],
                "draw": [],
                "disallowed_draw": [],
            })
            .to_string(),
        ),
        r#"201 {"circleid":"id2","name":"Circle 2"}"#,
    );

    // So they are in 2 circles each
    let mem_onl_circles = user_circles(get(&mut app, mem_onl, "/v1/circle").1);
    assert_eq!(mem_onl_circles.member_circles, vec!["id1", "id2"]);
    assert_eq!(mem_onl_circles.organiser_circles, Vec::<String>::new());

    let org_onl_circles = user_circles(get(&mut app, org_onl, "/v1/circle").1);
    assert_eq!(org_onl_circles.member_circles, Vec::<String>::new());
    assert_eq!(org_onl_circles.organiser_circles, vec!["id1", "id2"]);

    let mem_org_circles = user_circles(get(&mut app, mem_org, "/v1/circle").1);
    assert_eq!(mem_org_circles.member_circles, vec!["id1", "id2"]);
    assert_eq!(mem_org_circles.organiser_circles, vec!["id1", "id2"]);

    // When I delete one circle
    test_req(delete(&mut app, USRA, "/v1/circle/id1"), "204 ");

    // Then the people see themselves in only 1
    let mem_onl_circles = user_circles(get(&mut app, mem_onl, "/v1/circle").1);
    assert_eq!(mem_onl_circles.member_circles, vec!["id2"]);
    assert_eq!(mem_onl_circles.organiser_circles, Vec::<String>::new());

    let org_onl_circles = user_circles(get(&mut app, org_onl, "/v1/circle").1);
    assert_eq!(org_onl_circles.member_circles, Vec::<String>::new());
    assert_eq!(org_onl_circles.organiser_circles, vec!["id2"]);

    let mem_org_circles = user_circles(get(&mut app, mem_org, "/v1/circle").1);
    assert_eq!(mem_org_circles.member_circles, vec!["id2"]);
    assert_eq!(mem_org_circles.organiser_circles, vec!["id2"]);
}

#[test]
fn deleting_a_user_removes_them_from_circles() {
    // Given 2 circles with a and b as organiser and member

    let a = LogInAs { u: "a", p: "a" };
    let b = LogInAs { u: "b", p: "b" };

    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/user", &a.creation_json());
    post(&mut app, USRA, "/v1/user", &b.creation_json());

    test_req(
        post(
            &mut app,
            a,
            "/v1/circle",
            &json!( {
                "name": "Circle 1",
                "draw_date": "",
                "rules": "",
                "members": [a.mem(), b.mem()],
                "organisers": [a.un(), b.un()],
                "draw": [],
                "disallowed_draw": [],
            })
            .to_string(),
        ),
        r#"201 {"circleid":"id1","name":"Circle 1"}"#,
    );

    test_req(
        post(
            &mut app,
            b,
            "/v1/circle",
            &json!( {
                "name": "Circle 2",
                "draw_date": "",
                "rules": "",
                "members": [a.mem(), b.mem()],
                "organisers": [a.un(), b.un()],
                "draw": [],
                "disallowed_draw": [],
            })
            .to_string(),
        ),
        r#"201 {"circleid":"id2","name":"Circle 2"}"#,
    );

    // When I delete a
    test_req(delete(&mut app, USRA, "/v1/user/a"), "204 ");

    // Then b is the only member and organiser of both circles
    let b_circles = user_circles(get(&mut app, b, "/v1/circle").1);
    assert_eq!(b_circles.member_circles, vec!["id1", "id2"]);
    assert_eq!(b_circles.organiser_circles, vec!["id1", "id2"]);

    let (status, body) = get(&mut app, b, "/v1/circle/id1?view=organise");
    let body: serde_json::Value = serde_json::from_slice(&body).unwrap();
    assert_eq!(status, 200);
    assert_eq!(
        body.as_object().unwrap().get("members").unwrap(),
        &json! {[
            {
                "nickname": "",
                "username": "b",
                "wishlist_text": "",
                "wishlist_links": [],
            },
        ]}
    );
    assert_eq!(
        body.as_object().unwrap().get("organisers").unwrap(),
        &json! {[
            {
                "nickname": "",
                "username": "b",
            },
        ]}
    );

    let (status, body) = get(&mut app, b, "/v1/circle/id2?view=organise");
    let body: serde_json::Value = serde_json::from_slice(&body).unwrap();
    assert_eq!(status, 200);
    assert_eq!(
        body.as_object().unwrap().get("members").unwrap(),
        &json! {[
            {
                "nickname": "",
                "username": "b",
                "wishlist_text": "",
                "wishlist_links": [],
            },
        ]}
    );
    assert_eq!(
        body.as_object().unwrap().get("organisers").unwrap(),
        &json! {[
            {
                "nickname": "",
                "username": "b",
            },
        ]}
    );
}

#[test]
fn deleting_the_last_organiser_deletes_the_circle() {
    // Given a circle with a as an organiser and b as a member
    let a = LogInAs { u: "a", p: "a" };
    let b = LogInAs { u: "b", p: "b" };

    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/user", &a.creation_json());
    post(&mut app, USRA, "/v1/user", &b.creation_json());

    test_req(
        post(
            &mut app,
            a,
            "/v1/circle",
            &json!( {
                "name": "Circle 1",
                "draw_date": "",
                "rules": "",
                "members": [b.mem()],
                "organisers": [a.un()],
                "draw": [],
                "disallowed_draw": [],
            })
            .to_string(),
        ),
        r#"201 {"circleid":"id1","name":"Circle 1"}"#,
    );

    // When I delete a
    test_req(delete(&mut app, USRA, "/v1/user/a"), "204 ");

    // Then the circle is gone, because it has no organisers
    let b_circles = user_circles(get(&mut app, b, "/v1/circle").1);
    assert_eq!(b_circles.member_circles, Vec::<String>::new());

    let (status, _body) = get(&mut app, b, "/v1/circle/id1");
    assert_eq!(status, 404);
}

struct UserCircles {
    member_circles: Vec<String>,
    organiser_circles: Vec<String>,
}

fn user_circles(circle_resp: Bytes) -> UserCircles {
    let member_circles = parse(&circle_resp)
        .get("member_circles")
        .unwrap()
        .as_array()
        .unwrap()
        .iter()
        .map(|c| String::from(c.get("circleid").unwrap().as_str().unwrap()))
        .collect();

    let organiser_circles = parse(&circle_resp)
        .get("organiser_circles")
        .unwrap()
        .as_array()
        .unwrap()
        .iter()
        .map(|c| String::from(c.get("circleid").unwrap().as_str().unwrap()))
        .collect();

    UserCircles {
        member_circles,
        organiser_circles,
    }
}
