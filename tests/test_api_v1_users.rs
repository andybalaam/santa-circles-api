extern crate santa_circles_api;

mod utils;
mod utils_users;

use std::collections::HashSet;
use std::time::{Duration, SystemTime};

use santa_circles_api::real_token_generator::RealTokenGenerator;
use santa_circles_api::token_generator::TokenGenerator;

use utils::log_in_as::LogInAs;
use utils::new_app::{
    new_app, new_app_counting_saves, new_app_with_tokens,
    new_app_with_tokens_and_clock, FakeClock, FakeSaver, FakeTokenGenerator,
};
use utils::requests::{delete, get, get_nou, post, post_nou, put, test_req};
use utils_users::requests_users::{delete_with_payload, put_nou};

use crate::utils::new_app::{new_app_with_tokens_and_emailer, FakeEmailer};

const USR: LogInAs = LogInAs {
    u: "myuser",
    p: "mypass",
};

#[test]
fn initially_we_can_get_setup() {
    let mut app = new_app();
    test_req(get(&mut app, USR, "/v1/setup"), r#"204 "#);
}

#[test]
fn when_a_user_exists_get_setup_gives_404() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    test_req(get(&mut app, USR, "/v1/setup"), r#"404 "#);
}

#[test]
fn when_a_user_exists_post_setup_gives_404() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    test_req(
        post_nou(&mut app, "/v1/setup", &USR.creation_json()),
        r#"404 {"error":"Setup may only be run once."}"#,
    );
}

#[test]
fn newly_created_user_is_visible() {
    let saver = FakeSaver::new();
    let mut app = new_app_counting_saves(saver.clone());
    test_req(
        post_nou(&mut app, "/v1/setup", &USR.creation_json()),
        r#"201 {"username":"myuser","admin":true}"#,
    );
    assert_eq!(saver.num_saves(), 1);
    test_req(
        get(&mut app, USR, "/v1/user/myuser"),
        r#"200 {"username":"myuser","admin":true}"#,
    );
    assert_eq!(saver.num_saves(), 2);
}

#[test]
fn looking_at_my_info_with_incorrect_password_is_forbidden() {
    let mut app = new_app();
    test_req(
        post_nou(&mut app, "/v1/setup", &USR.creation_json()),
        r#"201 {"username":"myuser","admin":true}"#,
    );
    let wrong_password = LogInAs {
        u: "myuser",
        p: "WRONG",
    };

    test_req(
        get(&mut app, wrong_password, "/v1/user/myuser"),
        r#"401 Invalid username or password"#,
    );
}

#[test]
fn looking_at_my_info_with_no_auth_header_is_forbidden() {
    let mut app = new_app();
    test_req(
        post_nou(&mut app, "/v1/setup", &USR.creation_json()),
        r#"201 {"username":"myuser","admin":true}"#,
    );

    // Don't supply a username or password at all - should fail
    test_req(get_nou(&mut app, "/v1/user/myuser"), r#"401 "#);
}

#[test]
fn creating_two_users_with_same_username_is_an_error() {
    let mut app = new_app();
    test_req(
        post_nou(&mut app, "/v1/setup", &USR.creation_json()),
        r#"201 {"username":"myuser","admin":true}"#,
    );
    test_req(
        post(&mut app, USR, "/v1/user", &USR.creation_json()),
        r#"409 {"error":"User already exists."}"#,
    );
}

#[test]
fn first_user_can_create_new_users() {
    let mut app = new_app();
    test_req(
        post_nou(&mut app, "/v1/setup", &USR.creation_json()),
        r#"201 {"username":"myuser","admin":true}"#,
    );
    test_req(
        post(
            &mut app,
            USR,
            "/v1/user",
            r#"{"username": "myuser2", "password": "p2"}"#,
        ),
        r#"201 {"username":"myuser2","admin":false}"#,
    );
    test_req(
        post(
            &mut app,
            USR,
            "/v1/user",
            r#"{"username": "myuser3", "password": "p3", "admin":true}"#,
        ),
        r#"201 {"username":"myuser3","admin":true}"#,
    );
}

#[test]
fn admin_can_create_new_users() {
    let new_admin = LogInAs {
        u: "myadmin",
        p: "ap",
    };

    // Create an admin user
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post(
        &mut app,
        USR,
        "/v1/user",
        r#"{"username": "myadmin", "password": "ap", "admin":true}"#,
    );

    // That user can create other users
    test_req(
        post(
            &mut app,
            new_admin,
            "/v1/user",
            r#"{"username": "myuser2", "password": "p2", "admin":false}"#,
        ),
        r#"201 {"username":"myuser2","admin":false}"#,
    );
    test_req(
        post(
            &mut app,
            new_admin,
            "/v1/user",
            r#"{"username": "myuser3", "password": "p3", "admin":true}"#,
        ),
        r#"201 {"username":"myuser3","admin":true}"#,
    );
}

#[test]
fn admin_can_delete_users() {
    let new_admin = LogInAs {
        u: "myadmin",
        p: "ap",
    };

    // Create an admin user
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post(
        &mut app,
        USR,
        "/v1/user",
        r#"{"username": "myadmin", "password": "ap", "admin":true}"#,
    );

    // Create another user
    post(
        &mut app,
        new_admin,
        "/v1/user",
        r#"{"username": "myuser2", "password": "p2", "admin":true}"#,
    );

    // Admin can delete the new user
    test_req(delete(&mut app, new_admin, "/v1/user/myuser2"), r#"204 "#);

    // The user is gone
    test_req(
        get(&mut app, new_admin, "/v1/user/myuser2"),
        r#"404 {"error":"No such user."}"#,
    );
}

#[test]
fn nonadmin_cannot_delete_other_users() {
    let mut app = new_app();
    let myuser1 = LogInAs {
        u: "myuser1",
        p: "p",
    };
    let myuser2 = LogInAs {
        u: "myuser2",
        p: "p",
    };

    // Create admin and 2 users
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post(&mut app, USR, "/v1/user", &myuser1.creation_json());
    post(&mut app, USR, "/v1/user", &myuser2.creation_json());

    // One cannot delete the other
    test_req(
        delete(&mut app, myuser1, "/v1/user/myuser2"),
        r#"403 {"error":"Permission denied."}"#,
    );
}

#[test]
fn nonadmin_can_delete_themself() {
    let mut app = new_app();
    let myuser1 = LogInAs {
        u: "myuser1",
        p: "p",
    };

    // Create admin and normal user
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post(&mut app, USR, "/v1/user", &myuser1.creation_json());

    // User can delete themself
    test_req(delete(&mut app, myuser1, "/v1/user/myuser1"), r#"204 "#);

    // They are gone
    test_req(
        get(&mut app, USR, "/v1/user/myuser1"),
        r#"404 {"error":"No such user."}"#,
    );
}

#[test]
fn nonadmin_cannot_create_new_users() {
    let new_admin = LogInAs { u: "u1", p: "a1" };

    // Create a normal user
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post(
        &mut app,
        USR,
        "/v1/user",
        r#"{"username": "u1", "password": "a1", "admin":false}"#,
    );

    // That user cannot create other users
    test_req(
        post(
            &mut app,
            new_admin,
            "/v1/user",
            r#"{"username": "myuser2", "password": "p2", "admin":false}"#,
        ),
        r#"403 {"error":"Permission denied."}"#,
    );
    test_req(
        post(
            &mut app,
            new_admin,
            "/v1/user",
            r#"{"username": "myuser3", "password": "p3", "admin":true}"#,
        ),
        r#"403 {"error":"Permission denied."}"#,
    );
}

#[test]
fn nonexistent_user_is_not_visible() {
    let saver = FakeSaver::new();
    let mut app = new_app_counting_saves(saver.clone());
    // We have a user
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    assert_eq!(saver.num_saves(), 1);

    // But we ask for a different one
    test_req(
        get(&mut app, USR, "/v1/user/nonexistentuser"),
        r#"404 {"error":"No such user."}"#,
    );
    assert_eq!(saver.num_saves(), 2);
}

#[test]
fn nonadmin_asking_about_nonexistent_user_is_forbidden() {
    let nonadmin: LogInAs = LogInAs {
        u: "normal",
        p: "pn",
    };

    let saver = FakeSaver::new();
    let mut app = new_app_counting_saves(saver.clone());
    // We have a user
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post(&mut app, USR, "/v1/user", &nonadmin.creation_json());

    // But we ask for a different one
    test_req(
        get(&mut app, nonadmin, "/v1/user/nonexistentuser"),
        r#"403 {"error":"Permission denied."}"#,
    );
}

#[test]
fn a_valid_user_can_request_a_token() {
    // Create a user
    let token_generator = FakeTokenGenerator::new("faketoken");
    let mut app = new_app_with_tokens(token_generator);
    post_nou(&mut app, "/v1/setup", &USR.creation_json());

    // Request a token for that user - we should get one back
    test_req(
        post(&mut app, USR, "/v1/login", r#"{"remember_me":true}"#),
        r#"201 {"username":"myuser","token":"faketoken"}"#,
    );
}

#[test]
fn real_token_generator_returns_unique_tokens() {
    let token_generator = RealTokenGenerator::new();
    let mut tokens = HashSet::new();

    // Generate 100 tokens
    for _ in 1..=100 {
        tokens.insert(token_generator.generate_login_token());
    }

    // All the generated tokens are different
    assert_eq!(tokens.len(), 100)
}

#[test]
fn an_invalid_password_does_not_work_even_after_token_issued() {
    // Create a user
    let token_generator = FakeTokenGenerator::new("faketoken");
    let mut app = new_app_with_tokens(token_generator);
    post_nou(&mut app, "/v1/setup", &USR.creation_json());

    // Request a token for that user
    test_req(
        post(&mut app, USR, "/v1/login", r#"{"remember_me":true}"#),
        r#"201 {"username":"myuser","token":"faketoken"}"#,
    );

    // Attempt to work with an incorrect password
    let wrong_password = LogInAs {
        u: "myuser",
        p: "WRONG",
    };

    // It doesn't work
    test_req(
        get(&mut app, wrong_password, "/v1/user/myuser"),
        r#"401 Invalid username or password"#,
    );
}

#[test]
fn a_token_serves_instead_of_a_password() {
    // Create a user
    let token_generator = FakeTokenGenerator::new("tok");
    let mut app = new_app_with_tokens(token_generator);
    post_nou(&mut app, "/v1/setup", &USR.creation_json());

    // Request a token for that user
    test_req(
        post(&mut app, USR, "/v1/login", r#"{"remember_me":true}"#),
        r#"201 {"username":"myuser","token":"tok"}"#,
    );

    // Log in using the token as the password
    let token = LogInAs {
        u: "myuser",
        p: "tok",
    };

    // It works!
    test_req(
        get(&mut app, token, "/v1/user/myuser"),
        r#"200 {"username":"myuser","admin":true}"#,
    );
}

#[test]
fn after_a_token_is_deleted_it_does_not_work_any_more() {
    // Create a user and get a token for them
    let token_generator = FakeTokenGenerator::new("tok");
    let mut app = new_app_with_tokens(token_generator);
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post(&mut app, USR, "/v1/login", r#"{"remember_me":true}"#);
    let token = LogInAs {
        u: "myuser",
        p: "tok",
    };

    // We can log in using the token as the password
    test_req(
        get(&mut app, token, "/v1/user/myuser"),
        r#"200 {"username":"myuser","admin":true}"#,
    );

    // But after we delete it
    test_req(
        delete_with_payload(
            &mut app,
            token,
            "/v1/login/myuser",
            r#"{"token":"tok"}"#,
        ),
        r#"204 "#,
    );

    // Now we can't log in with it
    test_req(
        get(&mut app, token, "/v1/user/myuser"),
        r#"401 Invalid username or password"#,
    );

    // (But the user still exists and works.)
    test_req(
        get(&mut app, USR, "/v1/user/myuser"),
        r#"200 {"username":"myuser","admin":true}"#,
    );
}

#[test]
fn only_the_user_themself_can_delete_their_token() {
    let new_admin = LogInAs {
        u: "user2",
        p: "ap",
    };

    // Create 2 admin users
    let token_generator = FakeTokenGenerator::new("tok");
    let mut app = new_app_with_tokens(token_generator);
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post(
        &mut app,
        USR,
        "/v1/user",
        r#"{"username":"user2","password":"ap","admin":true}"#,
    );

    // Get a token for one of them
    post(&mut app, USR, "/v1/login", r#"{"remember_me":true}"#);

    // The other user is not allowed to delete it
    test_req(
        delete_with_payload(
            &mut app,
            new_admin,
            "/v1/login/myuser",
            r#"{"token":"tok"}"#,
        ),
        r#"403 {"error":"Permission denied."}"#,
    );
}

#[test]
fn my_token_only_works_for_me() {
    let user2 = LogInAs {
        u: "user2",
        p: "ap",
    };

    // Create 2 admin users
    let token_generator = FakeTokenGenerator::new("tok");
    let mut app = new_app_with_tokens(token_generator);
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post(&mut app, USR, "/v1/user", &user2.creation_json());

    // Get a token for the first user
    post(&mut app, USR, "/v1/login", r#"{"remember_me":true}"#);

    // The other user is not allowed to use it
    let token = LogInAs {
        u: "user2",
        p: "tok",
    };
    test_req(
        get(&mut app, token, "/v1/user/user2"),
        r#"401 Invalid username or password"#,
    );
}

#[test]
fn an_expired_token_does_not_gain_access() {
    // Create a user
    let token_generator = FakeTokenGenerator::new("tok");
    let mut clock = FakeClock::new(SystemTime::UNIX_EPOCH);
    let mut app = new_app_with_tokens_and_clock(token_generator, clock.clone());

    let token = LogInAs {
        u: "myuser",
        p: "tok",
    };

    post_nou(&mut app, "/v1/setup", &USR.creation_json());

    // Sanity: using the token does not work before we requested it
    test_req(
        get(&mut app, token, "/v1/user/myuser"),
        r#"401 Invalid username or password"#,
    );

    // Request a token for that user
    test_req(
        post(&mut app, USR, "/v1/login", r#"{"remember_me":true}"#),
        r#"201 {"username":"myuser","token":"tok"}"#,
    );

    // Using the token as the password works for requests
    test_req(
        get(&mut app, token, "/v1/user/myuser"),
        r#"200 {"username":"myuser","admin":true}"#,
    );

    // But later (after the 21-day expiration), it does not
    let twenty_two_days = Duration::from_secs(60 * 60 * 24 * 22);
    clock.advance_time(twenty_two_days);
    test_req(
        get(&mut app, token, "/v1/user/myuser"),
        r#"401 Invalid username or password"#,
    );
}

#[test]
fn remember_me_token_expires_after_21_days() {
    // Create a user
    let token_generator = FakeTokenGenerator::new("tok");
    let mut clock = FakeClock::new(SystemTime::UNIX_EPOCH);
    let mut app = new_app_with_tokens_and_clock(token_generator, clock.clone());
    let token = LogInAs {
        u: "myuser",
        p: "tok",
    };

    post_nou(&mut app, "/v1/setup", &USR.creation_json());

    test_req(
        post(&mut app, USR, "/v1/login", r#"{"remember_me":true}"#),
        r#"201 {"username":"myuser","token":"tok"}"#,
    );

    // After 20 days we can still do things
    clock.advance_time(Duration::from_secs(60 * 60 * 24 * 20));
    test_req(
        get(&mut app, token, "/v1/user/myuser"),
        r#"200 {"username":"myuser","admin":true}"#,
    );

    // But two days later, we cannot
    clock.advance_time(Duration::from_secs(60 * 60 * 24 * 2));
    test_req(
        get(&mut app, token, "/v1/user/myuser"),
        r#"401 Invalid username or password"#,
    );
}

#[test]
fn not_remember_me_token_expires_after_1_day() {
    // Create a user
    let token_generator = FakeTokenGenerator::new("tok");
    let mut clock = FakeClock::new(SystemTime::UNIX_EPOCH);
    let mut app = new_app_with_tokens_and_clock(token_generator, clock.clone());
    let token = LogInAs {
        u: "myuser",
        p: "tok",
    };

    post_nou(&mut app, "/v1/setup", &USR.creation_json());

    // Say "no" to "remember me"
    test_req(
        post(&mut app, USR, "/v1/login", r#"{"remember_me":false}"#),
        r#"201 {"username":"myuser","token":"tok"}"#,
    );

    // After 12 hours we can still do things
    clock.advance_time(Duration::from_secs(60 * 60 * 12));
    test_req(
        get(&mut app, token, "/v1/user/myuser"),
        r#"200 {"username":"myuser","admin":true}"#,
    );

    // But the next day, we cannot
    clock.advance_time(Duration::from_secs(60 * 60 * 25));
    test_req(
        get(&mut app, token, "/v1/user/myuser"),
        r#"401 Invalid username or password"#,
    );
}

#[test]
fn requesting_a_token_requires_password_not_token() {
    // Create a user
    let token_generator = FakeTokenGenerator::new("tok");
    let mut app = new_app_with_tokens(token_generator);
    post_nou(&mut app, "/v1/setup", &USR.creation_json());

    // Request a token for that user
    test_req(
        post(&mut app, USR, "/v1/login", r#"{"remember_me":true}"#),
        r#"201 {"username":"myuser","token":"tok"}"#,
    );

    // Request another token, using the first token - not allowed
    let token = LogInAs {
        u: "myuser",
        p: "tok",
    };
    test_req(
        post(&mut app, token, "/v1/login", r#"{"remember_me":true}"#),
        r#"401 Invalid username or password"#,
    );
}

#[test]
fn can_create_an_unregistered_user_without_logging_in() {
    // Given a set-up system
    let token_generator = FakeTokenGenerator::new("reg1");
    let mut app = new_app_with_tokens(token_generator);
    post_nou(&mut app, "/v1/setup", &USR.creation_json());

    // I can create a new, unregistered user without a login
    let user2 = LogInAs {
        u: "user2",
        p: "ap",
    };
    test_req(
        post_nou(&mut app, "/v1/newuser", &user2.creation_json()),
        r#"201 {"username":"user2","admin":false,"registration_code":"reg1"}"#,
    );
    // (Note registration code is included because we are using a DummyEmailer
    // here, so we can't send emails.)

    // The user is visible to admins
    test_req(
        get(&mut app, USR, "/v1/user/user2"),
        r#"200 {"username":"user2","admin":false}"#,
    );
}

#[test]
fn cannot_create_second_unregistered_user_with_same_username() {
    // Given an unregistered user
    let token_generator = FakeTokenGenerator::new("reg1");
    let mut app = new_app_with_tokens(token_generator);
    post_nou(&mut app, "/v1/setup", &USR.creation_json());

    let user2 = LogInAs {
        u: "user2",
        p: "ap",
    };
    test_req(
        post_nou(&mut app, "/v1/newuser", &user2.creation_json()),
        r#"201 {"username":"user2","admin":false,"registration_code":"reg1"}"#,
    );

    // When I try to create another user with the same username, it fails
    test_req(
        post_nou(&mut app, "/v1/newuser", &user2.creation_json()),
        r#"409 {"error":"User already exists."}"#,
    );
}

#[test]
fn cannot_register_a_user_with_space_in_their_username() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USR.creation_json());

    let user2 = LogInAs { u: "u ", p: "ap" };
    let user3 = LogInAs { u: " u", p: "ap" };
    let user4 = LogInAs { u: "u u", p: "ap" };
    test_req(
        post_nou(&mut app, "/v1/newuser", &user2.creation_json()),
        r#"400 {"error":"Invalid username - cannot contain spaces"}"#,
    );
    test_req(
        post_nou(&mut app, "/v1/newuser", &user3.creation_json()),
        r#"400 {"error":"Invalid username - cannot contain spaces"}"#,
    );
    test_req(
        post_nou(&mut app, "/v1/newuser", &user4.creation_json()),
        r#"400 {"error":"Invalid username - cannot contain spaces"}"#,
    );
}

#[test]
fn unregistered_user_cant_do_anything() {
    // Given an unregistered user
    let unr = LogInAs { u: "unr", p: "unp" };
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post_nou(&mut app, "/v1/newuser", &unr.creation_json());

    // The user can't look at themself or their lists
    test_req(
        get(&mut app, unr, "/v1/user/unr"),
        r#"403 {"error":"Permission denied."}"#,
    );
    test_req(
        get(&mut app, unr, "/v1/list/unr"),
        r#"403 {"error":"Permission denied."}"#,
    );
}

#[test]
fn can_request_registration_code_for_unregistered_user() {
    // Given an unregistered user
    let unr = LogInAs { u: "unr", p: "unp" };
    let token_generator = FakeTokenGenerator::new("reg1");
    let mut app = new_app_with_tokens(token_generator);
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post_nou(&mut app, "/v1/newuser", &unr.creation_json());

    // We can request a registration code without logging in
    test_req(
        get_nou(&mut app, "/v1/newuser/unr"),
        r#"200 {"registration_code":"reg1"}"#,
    );
}

#[test]
fn signing_up_with_an_existing_registered_username_is_an_error() {
    // Given an existing user
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USR.creation_json());

    // When we try to sign up as that user we see an error
    test_req(
        post_nou(&mut app, "/v1/newuser", &USR.creation_json()),
        r#"409 {"error":"User already exists."}"#,
    );
}

#[test]
fn signing_up_with_an_existing_unregistered_username_is_an_error() {
    // Given an existing unregistered user
    let unr = LogInAs { u: "unr", p: "unp" };
    let token_generator = FakeTokenGenerator::new("reg1");
    let mut app = new_app_with_tokens(token_generator);
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post_nou(&mut app, "/v1/newuser", &unr.creation_json());

    // When we try to sign up as that user we see an error
    test_req(
        post_nou(&mut app, "/v1/newuser", &unr.creation_json()),
        r#"409 {"error":"User already exists."}"#,
    );
}

#[test]
fn submitting_registration_code_makes_user_registered() {
    // Given an unregistered user
    let unr = LogInAs { u: "unr", p: "unp" };
    let token_generator = FakeTokenGenerator::new("reg1");
    let mut app = new_app_with_tokens(token_generator);
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post_nou(&mut app, "/v1/newuser", &unr.creation_json());

    // And we have requested a registration code
    test_req(
        get_nou(&mut app, "/v1/newuser/unr"),
        r#"200 {"registration_code":"reg1"}"#,
    );

    // When we send that code
    test_req(
        put_nou(
            &mut app,
            "/v1/newuser/unr",
            r#"{"registration_code":"reg1"}"#,
        ),
        r#"204 "#,
    );

    // We can get info about ourself, and we are now registered
    test_req(
        get(&mut app, unr, "/v1/user/unr"),
        r#"200 {"username":"unr","admin":false}"#, // (no "registration_code")
    );
}

#[test]
fn registration_code_is_emailed_to_user() {
    // Given a standard setup that is capable of sending email
    let unr = LogInAs {
        u: "unr@example.com",
        p: "unp",
    };
    let token_generator = FakeTokenGenerator::new("reg1");
    let emailer = FakeEmailer::new();
    let mut app =
        new_app_with_tokens_and_emailer(token_generator, emailer.clone());
    post_nou(&mut app, "/v1/setup", &USR.creation_json());

    // When we create a new user (the response doesn't contain reg code
    // because we can send emails.)
    test_req(
        post_nou(&mut app, "/v1/newuser", &unr.creation_json()),
        r#"201 {"username":"unr@example.com","admin":false,"registration_code":true}"#,
    );

    // (And user state doesn't contain the reg code either)
    test_req(
        get_nou(&mut app, "/v1/newuser/unr@example.com"),
        r#"200 {"registration_code":true}"#,
    );

    // Then an email is sent with the registration token
    let sent = emailer.sent();
    assert_eq!(sent.len(), 1);
    assert!(sent[0].contains("To: unr@example.com"));
    assert!(sent[0].contains("reg1"));
}

#[test]
fn incorrect_registration_code_has_no_effect() {
    // Given an unregistered user
    let unr = LogInAs { u: "unr", p: "unp" };
    let token_generator = FakeTokenGenerator::new("reg1");
    let mut app = new_app_with_tokens(token_generator);
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post_nou(&mut app, "/v1/newuser", &unr.creation_json());

    // And we have requested a registration code
    test_req(
        get_nou(&mut app, "/v1/newuser/unr"),
        r#"200 {"registration_code":"reg1"}"#,
    );

    // When we send an incorrect code
    test_req(
        put_nou(
            &mut app,
            "/v1/newuser/unr",
            r#"{"registration_code":"BAD_CODE"}"#,
        ),
        r#"400 {"error":"Incorrect registration code."}"#,
    );

    // We are still unregistered
    test_req(
        get(&mut app, unr, "/v1/user/unr"),
        r#"403 {"error":"Permission denied."}"#,
    );
    // And registration code has not changed
    test_req(
        get_nou(&mut app, "/v1/newuser/unr"),
        r#"200 {"registration_code":"reg1"}"#,
    );
}

#[test]
fn privacy_policy_is_available() {
    let mut app = new_app();
    test_req(
        get_nou(&mut app, "/v1/privacy"),
        r#"200 "<h2>PRIVACY_INFO</h2><p>foo</p>""#,
    );
}

#[test]
fn admin_can_change_own_password() {
    // Create a new user
    let mut app = new_app();
    test_req(
        post_nou(&mut app, "/v1/setup", &USR.creation_json()),
        r#"201 {"username":"myuser","admin":true}"#,
    );
    let wrong_password = LogInAs {
        u: "myuser",
        p: "new_password",
    };

    // Can't log in with new_password
    test_req(
        get(&mut app, wrong_password, "/v1/user/myuser"),
        r#"401 Invalid username or password"#,
    );

    // Change password
    test_req(
        put(
            &mut app,
            USR,
            "/v1/user/myuser",
            r#"{"password":"new_password"}"#,
        ),
        r#"200 {"username":"myuser","admin":true}"#,
    );

    // Now we can use the new password to log in
    test_req(
        get(&mut app, wrong_password, "/v1/user/myuser"),
        r#"200 {"username":"myuser","admin":true}"#,
    );

    // And we can't log in with the old one
    test_req(
        get(&mut app, USR, "/v1/user/myuser"),
        r#"401 Invalid username or password"#,
    );
}

#[test]
fn admin_can_update_other_user_to_from_admin() {
    let mut app = new_app();
    test_req(
        post_nou(&mut app, "/v1/setup", &USR.creation_json()),
        r#"201 {"username":"myuser","admin":true}"#,
    );

    let new_user = LogInAs {
        u: "new_user",
        p: "pw",
    };

    // Create user
    test_req(
        post(&mut app, USR, "/v1/user", &new_user.creation_json()),
        r#"201 {"username":"new_user","admin":false}"#,
    );

    // Admin can make them admin
    test_req(
        put(&mut app, USR, "/v1/user/new_user", r#"{"admin":true}"#),
        r#"200 {"username":"new_user","admin":true}"#,
    );

    // Now they are an admin
    test_req(
        get(&mut app, new_user, "/v1/user/new_user"),
        r#"200 {"username":"new_user","admin":true}"#,
    );

    // Admin can change their password
    test_req(
        put(
            &mut app,
            USR,
            "/v1/user/new_user",
            r#"{"password":"new_password"}"#,
        ),
        r#"200 {"username":"new_user","admin":true}"#,
    );

    // New password works
    let new_user_new_password = LogInAs {
        u: "new_user",
        p: "new_password",
    };
    test_req(
        get(&mut app, new_user_new_password, "/v1/user/new_user"),
        r#"200 {"username":"new_user","admin":true}"#,
    );
}

#[test]
fn nonadmin_cant_update_self_to_from_admin() {
    let mut app = new_app();
    test_req(
        post_nou(&mut app, "/v1/setup", &USR.creation_json()),
        r#"201 {"username":"myuser","admin":true}"#,
    );

    let new_user = LogInAs {
        u: "new_user",
        p: "pw",
    };

    // Create user
    test_req(
        post(&mut app, USR, "/v1/user", &new_user.creation_json()),
        r#"201 {"username":"new_user","admin":false}"#,
    );

    // They can change their own password
    test_req(
        put(
            &mut app,
            new_user,
            "/v1/user/new_user",
            r#"{"password":"new_password"}"#,
        ),
        r#"200 {"username":"new_user","admin":false}"#,
    );

    // New password works
    let new_user_new_password = LogInAs {
        u: "new_user",
        p: "new_password",
    };
    test_req(
        get(&mut app, new_user_new_password, "/v1/user/new_user"),
        r#"200 {"username":"new_user","admin":false}"#,
    );

    // They can't make themself admin
    test_req(
        put(
            &mut app,
            new_user_new_password,
            "/v1/user/new_user",
            r#"{"admin":true}"#,
        ),
        r#"403 {"error":"Permission denied."}"#,
    );
    test_req(
        get(&mut app, new_user_new_password, "/v1/user/new_user"),
        r#"200 {"username":"new_user","admin":false}"#,
    );
}

#[test]
fn can_request_and_apply_password_reset_for_registered_user() {
    // Given a user
    let tokens = FakeTokenGenerator::new("reset1");
    let emailer = FakeEmailer::new();
    let mut app = new_app_with_tokens_and_emailer(tokens, emailer.clone());
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post(
        &mut app,
        USR,
        "/v1/user",
        r#"{"username": "u1", "password": "p1"}"#,
    );

    // When I request a password reset
    test_req(
        post_nou(&mut app, "/v1/reset/u1", ""),
        r#"200 {"username":"u1"}"#,
    );

    // Then a reset password email is sent
    let sent = emailer.sent();
    assert_eq!(sent.len(), 1);
    assert!(sent[0].contains("password reset"));
    assert!(sent[0].contains("reset1"));

    // Now we request to reset the password
    test_req(
        post_nou(
            &mut app,
            "/v1/do_reset/u1",
            r#"{"password_reset_code":"reset1","new_password":"newpw"}"#,
        ),
        r#"200 {"username":"u1"}"#,
    );

    // Then logging in with the old password fails
    test_req(
        get(&mut app, LogInAs { u: "u1", p: "p1" }, "/v1/user/u1"),
        r#"401 Invalid username or password"#,
    );

    // And logging in with the new password succeeds
    test_req(
        get(
            &mut app,
            LogInAs {
                u: "u1",
                p: "newpw",
            },
            "/v1/user/u1",
        ),
        r#"200 {"username":"u1","admin":false}"#,
    );
}

#[test]
fn password_reset_code_does_not_leak() {
    // Given I have requested a password reset
    let tokens = FakeTokenGenerator::new("reset1");
    let emailer = FakeEmailer::new();
    let mut app = new_app_with_tokens_and_emailer(tokens, emailer.clone());
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post(
        &mut app,
        USR,
        "/v1/user",
        r#"{"username": "u1", "password": "p1"}"#,
    );
    test_req(
        post_nou(&mut app, "/v1/reset/u1", ""),
        r#"200 {"username":"u1"}"#,
    );

    // When I log in with the original password, the password reset code is
    // not returned with my user info.
    test_req(
        get(&mut app, LogInAs { u: "u1", p: "p1" }, "/v1/user/u1"),
        r#"200 {"username":"u1","admin":false}"#,
    );
}

#[test]
fn logging_in_deletes_password_reset_code() {
    // Given I have requested a password reset
    let tokens = FakeTokenGenerator::new("reset1");
    let emailer = FakeEmailer::new();
    let mut app = new_app_with_tokens_and_emailer(tokens, emailer.clone());
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post(
        &mut app,
        USR,
        "/v1/user",
        r#"{"username": "u1", "password": "p1"}"#,
    );
    test_req(
        post_nou(&mut app, "/v1/reset/u1", ""),
        r#"200 {"username":"u1"}"#,
    );

    // When I log in with the original password
    test_req(
        get(&mut app, LogInAs { u: "u1", p: "p1" }, "/v1/user/u1"),
        r#"200 {"username":"u1","admin":false}"#,
    );

    // Then trying to reset fails
    test_req(
        post_nou(
            &mut app,
            "/v1/do_reset/u1",
            r#"{"password_reset_code":"reset1","new_password":"newpw"}"#,
        ),
        r#"409 {"error":"Password reset has not been requested."}"#,
    );

    // And I can't log in with the new password
    test_req(
        get(
            &mut app,
            LogInAs {
                u: "u1",
                p: "newpw",
            },
            "/v1/user/u1",
        ),
        r#"401 Invalid username or password"#,
    );

    // And I can still log in with the old password
    test_req(
        get(&mut app, LogInAs { u: "u1", p: "p1" }, "/v1/user/u1"),
        r#"200 {"username":"u1","admin":false}"#,
    );
}

#[test]
fn incorrect_reset_code_does_not_reset_password() {
    // Given I have requested a password reset
    let tokens = FakeTokenGenerator::new("reset1");
    let emailer = FakeEmailer::new();
    let mut app = new_app_with_tokens_and_emailer(tokens, emailer.clone());
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post(
        &mut app,
        USR,
        "/v1/user",
        r#"{"username": "u1", "password": "p1"}"#,
    );
    test_req(
        post_nou(&mut app, "/v1/reset/u1", ""),
        r#"200 {"username":"u1"}"#,
    );

    // When I try to reset the password using the wrong reset code, it fails
    test_req(
        post_nou(
            &mut app,
            "/v1/do_reset/u1",
            r#"{"password_reset_code":"wrong_code","new_password":"newpw"}"#,
        ),
        r#"400 {"error":"Incorrect password reset code."}"#,
    );

    // Then logging in with the new password fails
    test_req(
        get(
            &mut app,
            LogInAs {
                u: "u1",
                p: "newpw",
            },
            "/v1/user/u1",
        ),
        r#"401 Invalid username or password"#,
    );

    // And logging in with the old password succeeds
    test_req(
        get(&mut app, LogInAs { u: "u1", p: "p1" }, "/v1/user/u1"),
        r#"200 {"username":"u1","admin":false}"#,
    );
}

#[test]
fn resetting_password_for_an_unregistered_user_updates_registers_them() {
    // Given an unregistered user
    let u = LogInAs { u: "u1", p: "p1" };
    let tokens = FakeTokenGenerator::new("reset1");
    let emailer = FakeEmailer::new();
    let mut app = new_app_with_tokens_and_emailer(tokens, emailer.clone());
    post_nou(&mut app, "/v1/setup", &USR.creation_json());
    post_nou(&mut app, "/v1/newuser", &u.creation_json());

    // When we request a password reset
    test_req(
        post_nou(&mut app, "/v1/reset/u1", ""),
        r#"200 {"username":"u1"}"#,
    );

    // And perform the reset
    test_req(
        post_nou(
            &mut app,
            "/v1/do_reset/u1",
            r#"{"password_reset_code":"reset1","new_password":"newpw"}"#,
        ),
        r#"200 {"username":"u1"}"#,
    );

    // Then we can log in and we are registered
    test_req(
        get(
            &mut app,
            LogInAs {
                u: "u1",
                p: "newpw",
            },
            "/v1/user/u1",
        ),
        r#"200 {"username":"u1","admin":false}"#,
    );

    // (And the old password doesn't work)
    test_req(
        get(&mut app, u, "/v1/user/u1"),
        r#"401 Invalid username or password"#,
    );
}
