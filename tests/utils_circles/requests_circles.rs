use actix_http::body::Body;
use actix_http::error::Error;
use actix_http::Request;
use actix_web::dev::{Service, ServiceResponse};
use actix_web::test;

use crate::utils::log_in_as::LogInAs;
use crate::utils::requests::req;

pub fn get_f64<'a, S>(app: &'a mut S, log_in_as: LogInAs, path: &str) -> f64
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    let res: serde_json::Value = serde_json::from_slice(
        &req(app, Some(log_in_as), test::TestRequest::get(), path).1,
    )
    .unwrap();
    res.as_f64().unwrap()
}
