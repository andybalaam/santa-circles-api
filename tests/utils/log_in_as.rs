use actix_web_httpauth::headers::authorization::Basic;

#[derive(Clone, Copy)]
pub struct LogInAs {
    pub u: &'static str,
    pub p: &'static str,
}

impl LogInAs {
    pub fn basic(&self) -> Basic {
        Basic::new(self.u, Some(self.p))
    }

    pub fn creation_json(&self) -> String {
        format!(r#"{{"username": "{}", "password":"{}"}}"#, self.u, self.p)
    }
}
