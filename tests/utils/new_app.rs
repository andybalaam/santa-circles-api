#![allow(dead_code)]

use actix_http::error::Error;
use actix_http::Request;
use actix_web::dev::{Service, ServiceResponse};
use actix_web::{test, web, App};
use futures::executor;
use santa_circles_api::real_emailer::DummyEmailer;
use std::cell::Cell;
use std::sync::{Arc, Mutex};
use std::time::{Duration, SystemTime};

use santa_circles_api;
use santa_circles_api::clock::Clock;
use santa_circles_api::db::memory_database::{Data, MemoryDatabase};
use santa_circles_api::emailer::Emailer;
use santa_circles_api::idgen::IdGen;
use santa_circles_api::privacy_page::PrivacyPage;
use santa_circles_api::save::Saver;
use santa_circles_api::state::State;
use santa_circles_api::token_generator::TokenGenerator;

use crate::utils::fake_password_handler::FakePasswordHandler;

const FAKE_PASSWORD_HANDLER: FakePasswordHandler = FakePasswordHandler {};

pub struct FakeIdGen {
    i: Mutex<Cell<u32>>,
}

impl FakeIdGen {
    pub fn new() -> FakeIdGen {
        FakeIdGen {
            i: Mutex::new(Cell::new(0)),
        }
    }
}

impl IdGen for FakeIdGen {
    fn gen(&self) -> String {
        let mut i = self.i.lock().unwrap();
        *i.get_mut() += 1;
        format!("id{}", i.get())
    }
}

#[derive(Clone)]
pub struct FakeSaver {
    pub save_counter: Arc<Mutex<Cell<u32>>>,
}

impl FakeSaver {
    pub fn new() -> FakeSaver {
        FakeSaver {
            save_counter: Arc::new(Mutex::new(Cell::new(0))),
        }
    }

    pub fn num_saves(&self) -> u32 {
        self.save_counter.lock().unwrap().get()
    }
}

impl Saver<Data> for FakeSaver {
    fn save(&self, _data: &Data) {
        let c = self.save_counter.lock().unwrap();
        c.set(c.get() + 1);
    }

    fn load(&self) -> Data {
        Data::new()
    }
}

pub struct FakeTokenGenerator {
    hardcoded_token: String,
}

impl FakeTokenGenerator {
    pub fn new(hardcoded_token: &str) -> FakeTokenGenerator {
        FakeTokenGenerator {
            hardcoded_token: String::from(hardcoded_token),
        }
    }
}

impl TokenGenerator for FakeTokenGenerator {
    fn generate_login_token(&self) -> String {
        self.hardcoded_token.clone()
    }

    fn generate_registration_code(&self) -> String {
        self.hardcoded_token.clone()
    }

    fn generate_password_reset_code(&self) -> String {
        self.hardcoded_token.clone()
    }
}

#[derive(Clone)]
pub struct FakeEmailer {
    content: Arc<Mutex<FakeEmailerContent>>,
}

impl FakeEmailer {
    pub fn new() -> Self {
        Self {
            content: Arc::new(Mutex::new(FakeEmailerContent::new())),
        }
    }

    pub fn sent(&self) -> Vec<String> {
        self.content.lock().unwrap().sent.clone()
    }
}

impl Emailer for FakeEmailer {
    fn can_send_email(&self) -> bool {
        // Obviously we don't really send emails in test, but we pretend
        // we can, and do the test equivalent, which is to remember what we
        // pretended to send.
        true
    }

    fn send_registration_code(
        &self,
        to_address: &str,
        registration_code: &str,
    ) -> Result<(), santa_circles_api::data::error::Error> {
        self.content.lock().unwrap().sent.push(format!(
            "To: {}\nYour registration code is {}.\n",
            to_address, registration_code
        ));
        Ok(())
    }

    fn send_password_reset_code(
        &self,
        to_address: &str,
        reset_code: &str,
    ) -> Result<(), santa_circles_api::data::Error> {
        self.content.lock().unwrap().sent.push(format!(
            "To: {}\nYour password reset code is {}.\n",
            to_address, reset_code
        ));
        Ok(())
    }
}

struct FakeEmailerContent {
    sent: Vec<String>,
}

impl FakeEmailerContent {
    fn new() -> Self {
        Self { sent: Vec::new() }
    }
}

#[derive(Clone)]
pub struct FakeClock {
    hardcoded_time: Arc<Mutex<Cell<SystemTime>>>,
}

impl FakeClock {
    pub fn new(hardcoded_time: SystemTime) -> FakeClock {
        FakeClock {
            hardcoded_time: Arc::new(Mutex::new(Cell::new(hardcoded_time))),
        }
    }

    pub fn advance_time(&mut self, duration: Duration) {
        *self.hardcoded_time.lock().unwrap().get_mut() += duration;
    }
}

impl Clock for FakeClock {
    fn now(&self) -> SystemTime {
        self.hardcoded_time.lock().unwrap().get().clone()
    }
}

pub fn new_app(
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    new_app_all(
        FakeSaver::new(),
        FakeIdGen::new(),
        FakeTokenGenerator::new(""),
        FakeClock::new(SystemTime::UNIX_EPOCH),
        DummyEmailer::new(),
    )
}

pub fn new_app_with_tokens(
    token_generator: FakeTokenGenerator,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    new_app_all(
        FakeSaver::new(),
        FakeIdGen::new(),
        token_generator,
        FakeClock::new(SystemTime::UNIX_EPOCH),
        DummyEmailer::new(),
    )
}

pub fn new_app_with_tokens_and_clock(
    token_generator: FakeTokenGenerator,
    clock: FakeClock,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    new_app_all(
        FakeSaver::new(),
        FakeIdGen::new(),
        token_generator,
        clock,
        DummyEmailer::new(),
    )
}

pub fn new_app_with_tokens_and_emailer(
    token_generator: FakeTokenGenerator,
    emailer: FakeEmailer,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    new_app_all(
        FakeSaver::new(),
        FakeIdGen::new(),
        token_generator,
        FakeClock::new(SystemTime::UNIX_EPOCH),
        emailer,
    )
}

pub fn new_app_counting_saves(
    saver: FakeSaver,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    new_app_all(
        saver,
        FakeIdGen::new(),
        FakeTokenGenerator::new(""),
        FakeClock::new(SystemTime::UNIX_EPOCH),
        DummyEmailer::new(),
    )
}

pub fn new_app_all<E: Emailer + 'static>(
    saver: FakeSaver,
    idgen: FakeIdGen,
    token_generator: FakeTokenGenerator,
    clock: FakeClock,
    emailer: E,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    executor::block_on(test::init_service(
        App::new()
            .app_data(web::Data::new(State::new(
                Box::new(MemoryDatabase::new(
                    Box::new(saver),
                    &FAKE_PASSWORD_HANDLER,
                    Box::new(idgen),
                )),
                Box::new(token_generator),
                Box::new(clock),
                Box::new(emailer),
                PrivacyPage::new(String::from(
                    "<h2>PRIVACY_INFO</h2><p>foo</p>",
                )),
            )))
            .configure(santa_circles_api::config_unauthenticated)
            .configure(santa_circles_api::config),
    ))
}
