all: test

test:
	cargo fmt
	cargo test

unit-test:
	cargo fmt
	cargo test --lib

run:
	cargo run

doc:
	rustup doc
	cargo doc --open

DEPL := santacirclesapi.artificialworlds.net:santa-circles-api

deploy:
	# Prerequisites:
	# rustup target add x86_64-unknown-linux-musl
	# sudo apt install musl-tools
	cargo build --target x86_64-unknown-linux-musl --release
	scp \
		target/x86_64-unknown-linux-musl/release/santa-circles-api \
		${DEPL}/santa-circles-api.new
	scp privacy-artificialworlds.html ${DEPL}/privacy.html
	scp password_reset_email-artificialworlds.txt ${DEPL}/password_reset_email.txt
	scp registration_email-artificialworlds.txt ${DEPL}/registration_email.txt
