# Santa Circles API

To use Santa Circles, visit
[santacircles.artificialworlds.net](https://santacircles.artificialworlds.net).

A RESTful server that allows you to add, update and query Santa Circles.

See https://gitlab.com/andybalaam/santa-circles for more.

## Prerequisites

[Install Rust](https://www.rust-lang.org/tools/install).

## Build

```bash
cargo test
```

## Run

```bash
cargo run
```

To change the interface or port to bind on, use something like:

```bash
cargo run -- --bind 127.0.0.1 8088
```
<!--
## Try it out live

There is a live version of the server running at
https://listsync.artificialworlds.net/api/v1/ - you can connect to it with
username "guest", password "guest", using HTTP Basic authentication.

For an easier introduction to the API, see the interactive documentation
(Swagger) at
[listsync.gitlab.io/listsync-spec](https://listsync.gitlab.io/listsync-spec).
Again, username/password are guest/guest.
-->

## Usage

See "Run" above for how to run the server locally.  Once it's running, you can
make requests as shown below.

Create the first user via the special `setup` path:

```bash
curl -v http://localhost:8088/v1/setup -H 'Content-Type: application/json' -d '{"username":"andy@example.com", "password":"pw"}'
```

Create a circle:

```bash
$ curl -v "-uandy@example.com:pw" http://localhost:8088/v1/circle -H 'Content-Type: application/json' -d '{"name":"My Circle","draw_date":"18th Dec","rules":"Free stuff only!","members":[{"username":"x@ex.uk","nickname":"X"},{"username":"y@ex.com","nickname":"Y"}],"organisers":[{"username":"andy@example.com","nickname":"Andy"}],"draw":[],"disallowed_draw":[]}'
{"circleid":"uDdW","name":"My Circle"}
```

Get circle info:

```bash
$ curl -v "-uandy@example.com:pw" http://localhost:8088/v1/circle/uDdW
{"circleid":"uDdW","name":"My Circle","draw_date":"18th Dec","rules":"Free stuff only!","members":[{"username":"x@ex.uk","nickname":"X","wishlist_text":"","wishlist_links":[]},{"username":"y@ex.com","nickname":"Y","wishlist_text":"","wishlist_links":[]}],"organisers":[{"username":"andy@example.com","nickname":"Andy"}],"recipients":null}
```

Update a circle:

```bash
$ curl -X PUT -v "-uandy@example.com:pw" http://localhost:8088/v1/circle/uDdW -H 'Content-Type: application/json' -d '{"name":"My Circle","draw_date":"18th Dec","rules":"Free stuff only!","members":[{"username":"x@ex.uk","nickname":"X"},{"username":"y@ex.com","nickname":"Y"},{"username":"z@ex.uk","nickname":"Z"}],"organisers":[{"username":"andy@example.com","nickname":"Andy"}],"draw":[],"disallowed_draw":[]}'
{"circleid":"uDdW","name":"My Circle"}
```

List all my circles:

```bash
$ curl -v "-uandy@example.com:pw" http://localhost:8088/v1/circle
{"username":"andy@example.com","member_circles":[],"organiser_circles":[{"circleid":"uDdW","name":"My Circle"},{"circleid":"4spv","name":"My Circle"}]}
```

## Security tokens

To avoid sending the password every time, you can create a time-limited security token:

```bash
curl -uandy@example.com:pw http://localhost:8088/v1/login -H 'Content-Type: application/json' -d '{"remember_me":false}'
```

This will provide a response like this:

```
{"username":"andy@example.com","token":"JVkJ4ag-cMuOQElKsk96AoQnxBO_HG11gMYLNMK7PTr3Pdznf7sCAhjakjxA9Jwn"}
```

Now you can use the supplied token in place of the password:

```bash
curl '-uandy@example.com:JVkJ4ag-cMuOQElKsk96AoQnxBO_HG11gMYLNMK7PTr3Pdznf7sCAhjakjxA9Jwn' http://localhost:8088/v1/circle/uDdW
```

This token will expire in 24 hours, or if you supply `{"remember_me":true}` in
the login request, it will expire in 21 days.

## Containerisation

There is a Dockerfile to build an arm64 image:

```
FROM debian:buster-slim

RUN mkdir /opt/santa-circles-api

COPY target/aarch64-unknown-linux-gnu/release/santa-circles-api /usr/local/bin/santa-circles-api

WORKDIR /opt/santa-circles-api (this folder should be mapped when the container is run, to allow persistent config files)
```

## Helm chart

To enable running in Kubernetes, a sample Helm chart has been provided (there is also a Dockerfile and Helm chart for santa circles client)

You should deploy the API and client into the same namespace.

Example values:

```
---
image: myrepo/santa-circles-api (the repo that contains the built image)
tag: latest (the image tag)
name: santa-circles-api (the name of the deployment)
namespace: santa-circles (the namespace - this needs to be created before deployment)
config: /data/santa-circles-api (the local folder to map)
```

The Kubernetes service. A ClusterIP service is configured that exposes port 8088 at cluster level.

The santa circles client then connects to the service using:

```
command: ["santa-circles"]
        args: ["--server_url", "http://santa-circles-api:8088"]
```

<!--
## listsync Spec

This server implements the API specified at
[listsync-spec](https://gitlab.com/listsync/listsync-spec).

## Clients

If you want to actually edit lists, you will need a client that can connect
to this server.  Try
[listsync-client-rust](https://gitlab.com/listsync/listsync-client-rust).
-->

## License

Copyright 2019-2022 Andy Balaam.

Released under the GNU AGPLv3, or later.  See [LICENSE](LICENSE).

## Code of conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md).  By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)
