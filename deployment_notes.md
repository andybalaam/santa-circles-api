## Notes to self about deployment.

Build and upload:

```bash
make deploy
```

There is a tmux running in my webspace.  To attach:

```bash
tmux attach
```

Make sure you are in the right screen for this program (screen 0), then press
Ctrl-c and:

```
mv santa-circles-api.new santa-circles-api
source config.env
./santa-circles-api
```

I have set up my web provider to proxy
https://santacirclesapi.artificialworlds.net to the process running in the
screen, on port 8088.

## Backup

The backup is run daily at 0123 UTC via cron on the filesandy user of my
server.  It copies the state database into a daily and monthly backup file.
