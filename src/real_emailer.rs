use lettre::{
    message::Mailbox, transport::smtp::authentication::Credentials, Message,
    SmtpTransport, Transport,
};

use crate::{data::Error, email_templates::EmailTemplates, emailer::Emailer};

const REGISTRATION_EMAIL_SUBJECT: &str = "Santa Circles Registration";
const PASSWORD_RESET_EMAIL_SUBJECT: &str = "Santa Circles Password Reset";

pub struct RealEmailer {
    email_templates: EmailTemplates,
    smtp_transport: SmtpTransport,
    from_address: Mailbox,
    cc_address: Option<Mailbox>,
}

impl RealEmailer {
    pub fn new(
        email_templates: EmailTemplates,
        email_relay: Option<String>,
        email_username: Option<String>,
        email_password: Option<String>,
        email_from_address: Option<String>,
        email_cc_address: Option<String>,
    ) -> Result<RealEmailer, String> {
        if let (
            Some(relay),
            Some(username),
            Some(password),
            Some(from_address),
        ) = (
            email_relay,
            email_username,
            email_password,
            email_from_address,
        ) {
            let credentials = Credentials::new(username, password);

            let smtp_transport = SmtpTransport::relay(&relay)
                .map_err(|e| e.to_string())?
                .credentials(credentials)
                .build();

            let from_address =
                from_address.parse::<Mailbox>().map_err(|e| e.to_string())?;

            let cc_address =
                email_cc_address.and_then(|addr| addr.parse::<Mailbox>().ok());

            Ok(RealEmailer {
                email_templates,
                smtp_transport,
                from_address,
                cc_address,
            })
        } else {
            Err("To send emails, you must supply --email-relay, \
                --email-username, --email-password and --email-from-address.  \
                They are not all present, so no emails will be sent."
                .to_owned())
        }
    }

    fn send_email(
        &self,
        to_address: &str,
        subject: &str,
        body: String,
        description: &str,
    ) -> Result<(), Error> {
        let email =
            Message::builder()
                .from(self.from_address.clone())
                .to(to_address
                    .parse::<Mailbox>()
                    .map_err(|e| Error::FailedToSendEmail(e.to_string()))?);

        let email = if let Some(cc_address) = &self.cc_address {
            email.cc(cc_address.clone())
        } else {
            email
        };

        let email = email
            .subject(subject)
            .body(body)
            .map_err(|e| Error::FailedToSendEmail(e.to_string()))?;

        self.smtp_transport
            .send(&email)
            .map_err(|e| Error::FailedToSendEmail(e.to_string()))?;

        println!("Sent {} email to {}", description, to_address);

        Ok(())
    }
}

impl Emailer for RealEmailer {
    fn can_send_email(&self) -> bool {
        true
    }

    fn send_registration_code(
        &self,
        to_address: &str,
        registration_code: &str,
    ) -> Result<(), Error> {
        self.send_email(
            to_address,
            REGISTRATION_EMAIL_SUBJECT,
            self.email_templates.registration(registration_code),
            "registration",
        )
    }

    fn send_password_reset_code(
        &self,
        to_address: &str,
        reset_code: &str,
    ) -> Result<(), Error> {
        self.send_email(
            to_address,
            PASSWORD_RESET_EMAIL_SUBJECT,
            self.email_templates.password_reset(reset_code),
            "password reset",
        )
    }
}

/**
 * An emailer that doesn't send emails.
 */
pub struct DummyEmailer {}

impl DummyEmailer {
    pub fn new() -> Self {
        Self {}
    }
}

impl Emailer for DummyEmailer {
    fn can_send_email(&self) -> bool {
        false
    }

    fn send_registration_code(
        &self,
        _to_address: &str,
        _registration_code: &str,
    ) -> Result<(), Error> {
        Ok(())
    }

    fn send_password_reset_code(
        &self,
        _to_address: &str,
        _reset_code: &str,
    ) -> Result<(), Error> {
        Ok(())
    }
}
