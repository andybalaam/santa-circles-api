use actix_web::HttpResponse;
use serde::Serialize;

use crate::data::Error;

#[derive(Serialize)]
struct Msg {
    error: String,
}

fn m(msg: &str) -> Msg {
    Msg {
        error: String::from(msg),
    }
}

fn ms(msg: String) -> Msg {
    Msg { error: msg }
}

pub fn from_error(error: Error) -> HttpResponse {
    match error {
        Error::CircleAlreadyExists => {
            HttpResponse::Conflict().json(m("Circle already exists."))
        }
        Error::CircleCreatorMustBeAnOrganiser => HttpResponse::BadRequest()
            .json(m("The user creating a circle must be an organiser.")),
        Error::CircleDoesNotExist => {
            HttpResponse::NotFound().json(m("Circle does not exist."))
        }
        Error::CircleMustHaveAnOrganiser => HttpResponse::BadRequest()
            .json(m("There must be at least one organiser for a circle.")),
        Error::ColumnDoesNotExist => {
            HttpResponse::NotFound().json(m("Column does not exist."))
        }
        Error::FailedToSendEmail(e) => HttpResponse::BadRequest()
            .json(ms(format!("Sending email failed! Error: {}", e))),
        Error::IncorrectRegistrationCode => {
            HttpResponse::BadRequest().json(m("Incorrect registration code."))
        }
        Error::IncorrectPasswordResetCode => {
            HttpResponse::BadRequest().json(m("Incorrect password reset code."))
        }
        Error::InternalError => HttpResponse::InternalServerError()
            .json(m("Internal server error.")),
        Error::ItemDoesNotExist => {
            HttpResponse::NotFound().json(m("Item does not exist."))
        }
        Error::ListDoesNotExist => {
            HttpResponse::NotFound().json(m("List does not exist."))
        }
        Error::ListAlreadyExists => {
            HttpResponse::Conflict().json(m("List already exists."))
        }
        Error::PasswordResetHasNotBeenRequested => HttpResponse::Conflict()
            .json(m("Password reset has not been requested.")),
        Error::SetupMayOnlyBeRunOnce => {
            HttpResponse::NotFound().json(m("Setup may only be run once."))
        }
        Error::TokenDoesNotExist => {
            HttpResponse::NotFound().json(m("Token does not exist"))
        }
        Error::Unauthorized => {
            HttpResponse::Forbidden().json(m("Permission denied."))
        }
        Error::UserDetailsInvalid(problem) => {
            HttpResponse::BadRequest().json(m(problem))
        }
        Error::UserDoesNotExist => {
            HttpResponse::NotFound().json(m("No such user."))
        }
        Error::UserAlreadyExists => {
            HttpResponse::Conflict().json(m("User already exists."))
        }
        Error::UserIsAlreadyRegistered => {
            HttpResponse::BadRequest().json(m("User is already registered."))
        }
        Error::WrongTypeForColumn(col, exp_type) => HttpResponse::BadRequest()
            .json(m(&format!(
                "Wrong type: the column '{}' needs values of type '{}'.",
                col, exp_type
            ))),
    }
}
