use rand::{prelude::SliceRandom, thread_rng};
use std::collections::HashMap;

use crate::data::{DrawItem, DrawWarning, Member};
use petgraph::{
    algo::hamiltonian_circuits_directed, graph::NodeIndex, visit::EdgeRef,
    Direction, Graph,
};

/// Given the supplied members list and list of disallowed relationships,
/// pick a set of relationships.
/// Normally, the result is a random valid draw, but if deterministic_draw is
/// true, the result will be predictable i.e. if you supply the same input you
/// will get the same return value.
/// If no valid draw is possible, a draw will still be made, and the returned
/// DrawOutcome will contain warnings explaining which rules from
/// disallowed_draw were broken.
/// If the list of warnings in the return value is empty, then the draw is
/// valid.
pub fn calculate(
    members: &Vec<Member>,
    disallowed_draw: &Vec<DrawItem>,
    deterministic_draw: bool,
) -> DrawOutcome {
    let mut shuffled: Vec<Member> = members.clone();
    if !deterministic_draw {
        shuffled.shuffle(&mut thread_rng());
    }
    calculate_draw(shuffled, disallowed_draw)
}

#[derive(Debug, PartialEq)]
pub struct DrawOutcome {
    pub draw: Vec<DrawItem>,
    pub draw_warnings: Vec<DrawWarning>,
}

impl DrawOutcome {
    fn new(draw: Vec<DrawItem>, warnings: Vec<DrawWarning>) -> Self {
        Self {
            draw,
            draw_warnings: warnings,
        }
    }
}

pub fn calculate_warnings(
    members: &Vec<Member>,
    disallowed_draw: &Vec<DrawItem>,
    draw: &Vec<DrawItem>,
) -> Vec<DrawWarning> {
    let mut warnings = Vec::new();

    for member in members {
        if member.username == "" {
            warnings.push(DrawWarning::NoUsername(member.nickname.clone()));
        }
    }

    let mut indices: HashMap<&str, NodeIndex<_>> = HashMap::new();
    let mut g: Graph<&str, ()> = Graph::new();
    for m in members {
        let n = g.add_node(&m.username);
        indices.insert(&m.username, n);
    }
    for draw_item in draw {
        let n1 = indices.get(draw_item.from_username.as_str());
        let n2 = indices.get(draw_item.to_username.as_str());

        match (n1, n2) {
            (Some(n1), Some(n2)) => {
                g.add_edge(n1.clone(), n2.clone(), ());
            }
            (Some(_), None) => {
                warnings.push(DrawWarning::EmptyTo(
                    draw_item.from_username.clone(),
                ));
            }
            (None, Some(_)) => {
                warnings.push(DrawWarning::EmptyFrom(
                    draw_item.to_username.clone(),
                ));
            }
            _ => {} // Both sides are empty - ignore
        };
    }

    for n in g.node_indices() {
        let out = g.edges_directed(n, Direction::Outgoing);
        match out.count() {
            0 => warnings.push(DrawWarning::NotGiving(String::from(g[n]))),
            c if c > 1 => {
                let out2 = g.edges_directed(n, Direction::Outgoing);
                warnings.push(DrawWarning::GivingMultiple(
                    String::from(g[n]),
                    out2.map(|e| String::from(g[e.target()])).collect(),
                ))
            }
            _ => {}
        }
        let inc = g.edges_directed(n, Direction::Incoming);
        match inc.count() {
            0 => warnings.push(DrawWarning::NotReceiving(String::from(g[n]))),
            c if c > 1 => {
                let inc2 = g.edges_directed(n, Direction::Incoming);
                warnings.push(DrawWarning::ReceivingMultiple(
                    String::from(g[n]),
                    inc2.map(|e| String::from(g[e.source()])).collect(),
                ))
            }
            _ => {}
        }
    }

    for draw_item in draw {
        if disallowed_draw.contains(draw_item) {
            warnings.push(DrawWarning::BreaksRestriction(draw_item.clone()));
        }
    }

    warnings
}

fn calculate_draw(
    members: Vec<Member>,
    disallowed_draw: &Vec<DrawItem>,
) -> DrawOutcome {
    let members_usernames: Vec<_> =
        members.iter().map(|m| m.username.as_str()).collect();
    let draw: Vec<DrawItem> = match members_usernames.len() {
        n if n < 2 => Vec::new(),
        n if n == 2 => two_people_draw(&members_usernames),
        _ => full_draw(&members_usernames, &disallowed_draw),
    };

    let warnings = calculate_warnings(&members, &disallowed_draw, &draw);
    DrawOutcome::new(draw, warnings)
}

fn two_people_draw(members: &Vec<&str>) -> Vec<DrawItem> {
    return vec![
        DrawItem::new(members[0], members[1]),
        DrawItem::new(members[1], members[0]),
    ];
}

fn full_draw(
    members: &Vec<&str>,
    disallowed_draw: &Vec<DrawItem>,
) -> Vec<DrawItem> {
    let mut g: Graph<&str, ()> = Graph::new();

    for m in members {
        g.add_node(m);
    }
    for n1 in g.node_indices() {
        for n2 in g.node_indices() {
            let di = DrawItem {
                from_username: String::from(g[n1]),
                to_username: String::from(g[n2]),
            };

            if n1 != n2 && !disallowed_draw.contains(&di) {
                g.add_edge(n1, n2, ());
            }
        }
    }

    let circuit = hamiltonian_circuits_directed(&g).next();

    match circuit {
        Some(c) => to_draw(&c, |&n| String::from(g[n])),
        None => to_draw(members, |&m| String::from(m)),
    }
}

fn to_draw<T, F>(path: &Vec<T>, f: F) -> Vec<DrawItem>
where
    T: Clone + PartialEq,
    F: Fn(&T) -> String,
{
    let mut ret = Vec::new();
    if let Some(mut prev_node) = path.last() {
        for node in path {
            if node == prev_node {
                break;
            }
            ret.push(DrawItem {
                from_username: f(prev_node),
                to_username: f(node),
            });
            prev_node = node;
        }
    }
    ret
}

#[cfg(test)]
mod test {
    use crate::{
        data::{DrawItem, Member},
        draw_calculation::{calculate_warnings, DrawOutcome, DrawWarning},
    };

    use super::calculate_draw;

    fn to_draw_items(input: &[(&str, &str)]) -> Vec<DrawItem> {
        input.iter().map(|(f, t)| DrawItem::new(*f, *t)).collect()
    }

    fn assert_draw(
        members: &[&str],
        disallowed_draw: &[(&str, &str)],
        expected: &[(&str, &str)],
    ) {
        let members = members
            .iter()
            .map(|m| Member {
                username: String::from(*m),
                nickname: String::from(""),
                wishlist_text: String::from(""),
                wishlist_links: Vec::new(),
            })
            .collect();
        let expected = DrawOutcome::new(to_draw_items(expected), Vec::new());
        let disallowed_draw = to_draw_items(disallowed_draw);

        assert_eq!(calculate_draw(members, &disallowed_draw), expected,);
    }

    fn assert_draw_failure(
        members: &[&str],
        disallowed_draw: &[(&str, &str)],
        expected_draw: &[(&str, &str)],
        expected_warnings: Vec<DrawWarning>,
    ) {
        let members = members
            .iter()
            .map(|m| Member {
                username: String::from(*m),
                nickname: String::from(""),
                wishlist_text: String::from(""),
                wishlist_links: Vec::new(),
            })
            .collect();
        let disallowed_draw = to_draw_items(disallowed_draw);
        let expected_draw = to_draw_items(expected_draw);
        let result = calculate_draw(members, &disallowed_draw);
        assert_eq!(result, DrawOutcome::new(expected_draw, expected_warnings,));
    }

    fn assert_warnings(
        members: &[&str],
        disallowed_draw: &[(&str, &str)],
        draw: &[(&str, &str)],
        expected_warnings: Vec<DrawWarning>,
    ) {
        let members = members
            .iter()
            .map(|m| Member {
                username: String::from(*m),
                nickname: String::from(""),
                wishlist_text: String::from(""),
                wishlist_links: Vec::new(),
            })
            .collect();
        let disallowed_draw = to_draw_items(disallowed_draw);
        let draw = to_draw_items(draw);
        let warnings = calculate_warnings(&members, &disallowed_draw, &draw);
        assert_eq!(warnings, expected_warnings);
    }

    #[test]
    fn empty_members_gives_empty_draw() {
        assert_draw(&[], &[], &[]);
    }

    #[test]
    fn two_members_draw_each_other() {
        assert_draw(&["a", "b"], &[], &[("a", "b"), ("b", "a")]);
    }

    #[test]
    fn no_restrictions_means_a_nice_circle() {
        assert_draw(
            &["a", "b", "c", "d"],
            &[],
            &[("b", "a"), ("a", "d"), ("d", "c"), ("c", "b")],
        );
    }

    #[test]
    fn not_enough_connections_means_error() {
        // All paths out of a are disalllowed
        assert_draw_failure(
            &["a", "b", "c"],
            &[("a", "b"), ("a", "c")],
            &[("c", "a"), ("a", "b"), ("b", "c")],
            vec![DrawWarning::BreaksRestriction(DrawItem::new("a", "b"))],
        );
    }

    #[test]
    fn only_one_member_means_error() {
        assert_draw_failure(
            &["a"],
            &[],
            &[],
            vec![
                DrawWarning::NotGiving(String::from("a")),
                DrawWarning::NotReceiving(String::from("a")),
            ],
        );
    }

    #[test]
    fn draw_warnings_report_various_failures() {
        assert_warnings(
            &["a", "b", "c", "d"],
            &[("a", "c")],
            &[("a", "c"), ("c", "a"), ("c", "d"), ("d", "a")],
            vec![
                DrawWarning::ReceivingMultiple(
                    String::from("a"),
                    vec![String::from("d"), String::from("c")],
                ),
                DrawWarning::NotGiving(String::from("b")),
                DrawWarning::NotReceiving(String::from("b")),
                DrawWarning::GivingMultiple(
                    String::from("c"),
                    vec![String::from("d"), String::from("a")],
                ),
                DrawWarning::BreaksRestriction(DrawItem::new("a", "c")),
            ],
        );
    }

    #[test]
    fn missing_from_or_to_in_draw_item_is_reported_as_warning() {
        assert_warnings(
            &["a", "b"],
            &[],
            &[("a", ""), ("", "b"), ("a", "b"), ("b", "a")],
            vec![
                DrawWarning::EmptyTo(String::from("a")),
                DrawWarning::EmptyFrom(String::from("b")),
            ],
        );
    }
}
