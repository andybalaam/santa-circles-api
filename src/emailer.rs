use crate::data::error::Error;

pub trait Emailer: Send + Sync {
    /// Return true if this emailer is capable of sending email.
    /// Will be false if we were not launched with the email server config.
    fn can_send_email(&self) -> bool;

    fn send_registration_code(
        &self,
        to_address: &str,
        registration_code: &str,
    ) -> Result<(), Error>;

    fn send_password_reset_code(
        &self,
        to_address: &str,
        reset_code: &str,
    ) -> Result<(), Error>;
}
