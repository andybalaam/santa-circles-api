use serde::{Deserialize, Serialize};
use std::time::SystemTime;

use crate::data::{
    CircleInfo, CircleMemberInfo, CircleOrganiserInfo, CirclesInfo, DrawItem,
    DrawWarning, Error, ListInfo, ListItem, Member, NewItem, NewUser,
    ReplacementItem, UpdateUser, UserResponse, UserTokenResponse, UsernameNick,
};
use crate::db::memory_database::{Circles, Users};
use crate::db::Database;
use crate::idgen::IdGen;
use crate::password_handler::PasswordHandler;
use crate::save::{SaveOnChange, SaveOnChangeMutGuard, Saver};

#[derive(Default, Deserialize, Serialize)]
pub struct Data {
    users: Users,
    circles: Circles,
}

impl Data {
    pub fn new() -> Self {
        Self {
            users: Users::new(),
            circles: Circles::new(),
        }
    }
}

pub struct MemoryDatabase {
    data: SaveOnChange<Data>,
    password_handler: &'static dyn PasswordHandler,
    idgen: Box<dyn IdGen>,
}

impl MemoryDatabase {
    pub fn new(
        saver: Box<dyn Saver<Data>>,
        password_handler: &'static dyn PasswordHandler,
        idgen: Box<dyn IdGen>,
    ) -> MemoryDatabase {
        MemoryDatabase {
            data: SaveOnChange::new(saver.load(), saver),
            password_handler,
            idgen,
        }
    }

    fn delete_password_reset_code(&self, username: &str) {
        let _ = self
            .data
            .write_lock()
            .users
            .get_mut(username)
            .map(|user| user.info.password_reset_code = None);
    }
}

impl Database for MemoryDatabase {
    fn is_correct_password(&self, username: &str, password: &str) -> bool {
        let correct = self
            .data
            .read_lock()
            .users
            .get(username)
            .map(|user| {
                self.password_handler
                    .is_correct_password(password, &user.info.hashed_password)
            })
            .unwrap_or(false);

        if correct {
            // Every time we successfully log in, we delete any password
            // reset code that has been created.
            self.delete_password_reset_code(username);
        }

        correct
    }

    fn is_correct_token(
        &self,
        username: &str,
        token: &str,
        now: SystemTime,
    ) -> bool {
        // We use nonsaving_write_lock here because we may modify the DB if
        // tokens have expired, but we don't care if that change is lost
        // and we have to re-do it next time.
        let correct = self
            .data
            .nonsaving_write_lock()
            .users
            .get_mut(username)
            .map(|user| user.tokens.is_valid(token, now))
            .unwrap_or(false);

        if correct {
            // Every time we successfully log in, we delete any password
            // reset code that has been created.
            self.delete_password_reset_code(username);
        }

        correct
    }

    fn add_circle(
        &self,
        username: &str,
        circle_name: &str,
        draw_date: &str,
        rules: &str,
        members: &[Member],
        organisers: &[UsernameNick],
        draw: &[DrawItem],
        disallowed_draw: &[DrawItem],
        draw_warnings: &[DrawWarning],
    ) -> Result<CircleInfo, Error> {
        // TODO: This logic doesn't live inside a specific DB impl - it should
        //       be inside the API code - but we leave it here because it
        //       should happpen inside a single DB transaction, to prevent
        //       data inconsistencies.

        if !organisers.iter().any(|un| un.username == username) {
            return Err(Error::CircleCreatorMustBeAnOrganiser);
        }

        let circleid = self.idgen.gen();
        let mut data = self.data.write_lock();

        for username in members.iter().map(|un| &un.username) {
            let _ignore_notexisting = data
                .users
                .get_mut(username)
                .map(|user| user.member_circleids.push(circleid.clone()));
        }

        for username in organisers.iter().map(|un| &un.username) {
            let _ignore_notexisting = data
                .users
                .get_mut(username)
                .map(|user| user.organiser_circleids.push(circleid.clone()));
        }

        data.circles.add(
            circleid,
            circle_name,
            draw_date,
            rules,
            members,
            organisers,
            draw,
            disallowed_draw,
            draw_warnings,
        )
    }

    fn update_circle(
        &self,
        circleid: &str,
        circle_name: &str,
        draw_date: &str,
        rules: &str,
        members: &[Member],
        organisers: &[UsernameNick],
        draw: &[DrawItem],
        disallowed_draw: &[DrawItem],
        draw_warnings: &[DrawWarning],
    ) -> Result<CircleInfo, Error> {
        // TODO: This logic doesn't live inside a specific DB impl - it should
        //       be inside the API code - but we leave it here because it
        //       should happpen inside a single DB transaction, to prevent
        //       data inconsistencies.

        if organisers.is_empty() {
            return Err(Error::CircleMustHaveAnOrganiser);
        }

        let mut data = self.data.write_lock();
        let circle = data.circles.get(circleid)?;

        let added_members = difference_mem(members, &circle.members);
        let removed_members = difference_mem(&circle.members, members);
        let added_organisers = difference(organisers, &circle.organisers);
        let removed_organisers = difference(&circle.organisers, organisers);

        for username in removed_members {
            let _ignore_notexisting =
                data.users.get_mut(&username).map(|user| {
                    let idx = user
                        .member_circleids
                        .iter()
                        .position(|ci| ci == &circleid);
                    if let Some(idx) = idx {
                        user.member_circleids.remove(idx);
                    }
                });
        }

        for username in removed_organisers {
            let _ignore_notexisting =
                data.users.get_mut(&username).map(|user| {
                    let idx = user
                        .organiser_circleids
                        .iter()
                        .position(|ci| ci == &circleid);
                    if let Some(idx) = idx {
                        user.organiser_circleids.remove(idx);
                    }
                });
        }

        for username in added_members {
            let _ignore_notexisting = data
                .users
                .get_mut(&username)
                .map(|user| user.member_circleids.push(String::from(circleid)));
        }

        for username in added_organisers {
            let _ignore_notexisting =
                data.users.get_mut(&username).map(|user| {
                    user.organiser_circleids.push(String::from(circleid))
                });
        }

        let mut circle = data.circles.get_mut(circleid)?;
        circle.info.name = String::from(circle_name);
        circle.draw_date = String::from(draw_date);
        circle.rules = String::from(rules);
        circle.members = members.into();
        circle.organisers = organisers.into();
        circle.draw = draw.into();
        circle.disallowed_draw = disallowed_draw.into();
        circle.draw_warnings = draw_warnings.into();

        Ok(circle.info.clone())
    }

    fn add_user(
        &self,
        new_user: &NewUser,
        registration_code: Option<String>,
    ) -> Result<UserResponse, Error> {
        // TODO: This logic doesn't live inside a specific DB impl - it should
        //       be inside the API code - but we leave it here because it
        //       should happpen inside a single DB transaction, to prevent
        //       data inconsistencies.

        let mut data = self.data.write_lock();

        let ret = data
            .users
            .add(
                &new_user.username,
                self.password_handler.hash_password(&new_user.password),
                new_user.admin,
                registration_code,
            )
            .map(|user_info| user_info.to_response());

        update_new_user_memberships(data, &new_user.username)?;

        ret
    }

    fn update_user(
        &self,
        username: &str,
        update_user: &UpdateUser,
    ) -> Result<UserResponse, Error> {
        self.data
            .write_lock()
            .users
            .update(
                username,
                match &update_user.password {
                    None => None,
                    Some(pw) => Some(self.password_handler.hash_password(&pw)),
                },
                update_user.admin,
                &update_user.reset_code,
            )
            .map(|user_info| user_info.to_response())
    }

    fn add_user_token(
        &self,
        username: &str,
        token: &str,
        expiry_time: SystemTime,
    ) -> Result<UserTokenResponse, Error> {
        self.data
            .write_lock()
            .users
            .get_mut(username)
            .map(|user| user.tokens.add(username, token, expiry_time))
    }

    fn delete_user_token(
        &self,
        username: &str,
        token: &str,
    ) -> Result<(), Error> {
        self.data
            .write_lock()
            .users
            .get_mut(username)
            .and_then(|user| user.tokens.delete(token))
    }

    fn set_user_registered(&self, username: &str) -> Result<(), Error> {
        self.data
            .write_lock()
            .users
            .get_mut(username)
            .map(|user| user.info.registration_code = None)
    }

    fn delete_user(&self, username: &str) -> Result<(), Error> {
        // TODO: This logic doesn't live inside a specific DB impl - it should
        //       be inside the API code - but we leave it here because it
        //       should happpen inside a single DB transaction, to prevent
        //       data inconsistencies.

        let mut data = self.data.write_lock();

        let ret = data.users.delete(&username);

        update_deleted_user_circles(data, &username);

        ret
    }

    fn count_users(&self) -> usize {
        self.data.read_lock().users.len()
    }

    fn get_user(&self, username: &str) -> Result<UserResponse, Error> {
        self.data
            .read_lock()
            .users
            .get(username)
            .map(|user| user.info.to_response())
    }

    fn get_circles(&self, username: &str) -> Result<CirclesInfo, Error> {
        let data = self.data.read_lock();
        let user = data.users.get(username)?;

        let member_circles = user
            .member_circleids
            .iter()
            .filter_map(|circleid| data.circles.get(circleid).ok())
            .map(|circle| circle.info.clone())
            .collect();
        let organiser_circles = user
            .organiser_circleids
            .iter()
            .filter_map(|circleid| data.circles.get(circleid).ok())
            .map(|circle| circle.info.clone())
            .collect();

        Ok(CirclesInfo {
            username: String::from(username),
            member_circles,
            organiser_circles,
        })
    }

    fn delete_circle(&self, circleid: &str) -> Result<(), Error> {
        self.data.write_lock().circles.delete(circleid)
    }

    fn get_circle_member_info(
        &self,
        circleid: &str,
        username: &str,
    ) -> Result<CircleMemberInfo, Error> {
        self.data
            .read_lock()
            .circles
            .get(circleid)
            .map(|c| c.member_info(username))
    }

    fn get_circle_organiser_info(
        &self,
        circleid: &str,
    ) -> Result<CircleOrganiserInfo, Error> {
        self.data
            .read_lock()
            .circles
            .get(circleid)
            .map(|c| c.into())
    }

    fn get_lists(&self, username: &str) -> Result<Vec<ListInfo>, Error> {
        self.data
            .read_lock()
            .users
            .get(username)
            .map(|user| user.lists.all())
    }

    fn add_list(&self, username: &str, title: &str) -> Result<ListInfo, Error> {
        self.data.write_lock().users.get_mut(username).map(|user| {
            user.lists.add(user.generate_listid(&*self.idgen), title)
        })
    }

    fn update_list(
        &self,
        username: &str,
        listid: &str,
        title: &str,
    ) -> Result<ListInfo, Error> {
        self.data
            .write_lock()
            .users
            .get_mut(username)
            .and_then(|user| {
                user.lists.get_mut(listid).map(|list| {
                    list.info.title = String::from(title);
                    list.info.clone()
                })
            })
    }

    fn delete_list(&self, username: &str, listid: &str) -> Result<(), Error> {
        self.data
            .write_lock()
            .users
            .get_mut(username)
            .and_then(|user| user.lists.delete(listid))
    }

    fn get_list(
        &self,
        username: &str,
        listid: &str,
    ) -> Result<ListInfo, Error> {
        self.data
            .read_lock()
            .users
            .get(username)
            .and_then(|user| user.lists.get(listid))
            .map(|list| list.info.clone())
    }

    fn set_items(
        &self,
        username: &str,
        listid: &str,
        new_items: &Vec<NewItem>,
    ) -> Result<Vec<ListItem>, Error> {
        self.data
            .write_lock()
            .users
            .get_mut(username)
            .and_then(|user| user.lists.get_mut(listid))
            .map(|list| list.replace_all(new_items).clone())
    }

    fn get_items(
        &self,
        username: &str,
        listid: &str,
    ) -> Result<Vec<ListItem>, Error> {
        self.data
            .read_lock()
            .users
            .get(username)
            .and_then(|user| user.lists.get(listid))
            .map(|list| list.items.clone())
    }

    fn add_item(
        &self,
        username: &str,
        listid: &str,
        new_item: &NewItem,
    ) -> Result<ListItem, Error> {
        self.data
            .write_lock()
            .users
            .get_mut(username)
            .and_then(|user| user.lists.get_mut(listid))
            .map(|list| list.add(new_item, &vec![]))
    }

    fn delete_item(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
    ) -> Result<(), Error> {
        self.data
            .write_lock()
            .users
            .get_mut(username)
            .and_then(|user| user.lists.get_mut(listid))
            .and_then(|list| list.delete(itemid))
    }

    fn get_item(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
    ) -> Result<ListItem, Error> {
        self.data
            .read_lock()
            .users
            .get(username)
            .and_then(|user| user.lists.get(listid))
            .and_then(|list| list.find(itemid))
    }

    fn set_item(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
        replacement_item: &ReplacementItem,
    ) -> Result<ListItem, Error> {
        self.data
            .write_lock()
            .users
            .get_mut(username)
            .and_then(|user| user.lists.get_mut(listid))
            .and_then(|list| {
                list.find(itemid).and_then(|_| {
                    list.replace(
                        itemid,
                        replacement_item.order,
                        replacement_item.ticked,
                        &replacement_item.text,
                    )
                })
            })
    }

    fn get_cell(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
        columnname: &str,
    ) -> Result<serde_json::Value, Error> {
        self.data
            .read_lock()
            .users
            .get(username)
            .and_then(|user| user.lists.get(listid))
            .and_then(|list| list.find(itemid))
            .and_then(|item| item.get(columnname))
    }

    fn set_cell(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
        columnname: &str,
        value: &serde_json::Value,
    ) -> Result<(), Error> {
        self.data
            .write_lock()
            .users
            .get_mut(username)
            .and_then(|user| user.lists.get_mut(listid))
            .and_then(|list| {
                let new_item: Result<ListItem, Error> = list
                    .find(itemid)
                    .and_then(|mut item_info| item_info.set(columnname, value));
                new_item.and_then(|item| {
                    list.replace(itemid, item.order, item.ticked, &item.text)
                })
            })
            .map(|_| ())
    }
}

/**
 * Given we have just deleted a user with the supplied username, search for
 * circles that list them as a member or organiser, and remove them.
 */
fn update_deleted_user_circles(
    mut data: SaveOnChangeMutGuard<Data>,
    username: &str,
) {
    let mut circles_to_delete = Vec::new();

    for circle in data.circles.iter_mut() {
        circle.members.retain(|m| m.username != username);
        circle.organisers.retain(|m| m.username != username);
        if circle.organisers.len() == 0 {
            circles_to_delete.push(circle.info.circleid.clone());
        }
    }

    for circleid in circles_to_delete {
        let _ = data.circles.delete(&circleid);
    }
}

/**
 * Given we have just created a user with the supplied username, search for
 * circles that already list them as a member or organiser, and update this
 * user to reflect those relationships.
 */
fn update_new_user_memberships(
    mut data: SaveOnChangeMutGuard<Data>,
    username: &str,
) -> Result<(), Error> {
    let mut member_circleids = Vec::new();
    let mut organiser_circleids = Vec::new();

    for circle in data.circles.iter() {
        if circle.members.iter().any(|un| un.username == username) {
            member_circleids.push(circle.info.circleid.clone())
        }
        if circle.organisers.iter().any(|un| un.username == username) {
            organiser_circleids.push(circle.info.circleid.clone())
        }
    }

    data.users.get_mut(username).map(|user| {
        user.member_circleids = member_circleids;
        user.organiser_circleids = organiser_circleids;
    })
}

fn difference_mem(left: &[Member], right: &[Member]) -> Vec<String> {
    left.iter()
        .filter(|m1| right.iter().all(|m2| m2.username != m1.username))
        .map(|m| m.username.clone())
        .collect()
}

fn difference(left: &[UsernameNick], right: &[UsernameNick]) -> Vec<String> {
    left.iter()
        .filter(|unl| right.iter().all(|unr| unr.username != unl.username))
        .map(|un| un.username.clone())
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;

    fn uns(us: &[&str]) -> Vec<UsernameNick> {
        us.iter()
            .map(|&u| UsernameNick {
                username: String::from(u),
                nickname: String::from(""),
            })
            .collect()
    }

    fn ss(us: &[&str]) -> Vec<String> {
        us.iter().map(|&u| String::from(u)).collect()
    }

    #[test]
    fn diff_empty_vecs() {
        assert_eq!(difference(&uns(&[]), &uns(&[])), ss(&[]));
    }

    #[test]
    fn diff_removes_existing() {
        assert_eq!(
            difference(&uns(&["a", "b", "c", "d"]), &uns(&["b", "c"])),
            ss(&["a", "d"])
        );
    }

    #[test]
    fn diff_ignores_nonexisting() {
        assert_eq!(
            difference(&uns(&["a", "b", "c", "d"]), &uns(&["e", "f"])),
            ss(&["a", "b", "c", "d"])
        );
    }
}
