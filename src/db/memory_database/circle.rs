use serde::{Deserialize, Serialize};

use crate::data::{
    CircleInfo, CircleMemberInfo, CircleOrganiserInfo, DrawItem, DrawWarning,
    Member, UsernameNick,
};

#[derive(Deserialize, Serialize)]
pub struct Circle {
    pub info: CircleInfo,
    pub draw_date: String,
    pub rules: String,
    pub members: Vec<Member>,
    pub organisers: Vec<UsernameNick>,
    pub draw: Vec<DrawItem>,
    pub disallowed_draw: Vec<DrawItem>,
    pub draw_warnings: Vec<DrawWarning>,
}

impl Circle {
    pub fn new(
        info: CircleInfo,
        draw_date: String,
        rules: String,
        members: Vec<Member>,
        organisers: Vec<UsernameNick>,
        draw: Vec<DrawItem>,
        disallowed_draw: Vec<DrawItem>,
        draw_warnings: Vec<DrawWarning>,
    ) -> Circle {
        Circle {
            info,
            draw_date,
            rules,
            members,
            organisers,
            draw,
            disallowed_draw,
            draw_warnings,
        }
    }

    pub fn member_info(&self, username: &str) -> CircleMemberInfo {
        CircleMemberInfo {
            name: self.info.name.clone(),
            draw_date: self.draw_date.clone(),
            rules: self.rules.clone(),
            circleid: self.info.circleid.clone(),
            members: self.members.clone(),
            organisers: self.organisers.clone(),
            recipients: member_recipients(self, username),
        }
    }
}

fn member_recipients(circle: &Circle, username: &str) -> Option<Vec<String>> {
    if circle.draw.is_empty() {
        None
    } else {
        Some(
            circle
                .draw
                .iter()
                .filter_map(|item| {
                    if item.from_username == username {
                        Some(item.to_username.clone())
                    } else {
                        None
                    }
                })
                .collect(),
        )
    }
}

impl From<&Circle> for CircleOrganiserInfo {
    fn from(circle: &Circle) -> Self {
        Self {
            circleid: circle.info.circleid.clone(),
            name: circle.info.name.clone(),
            draw_date: circle.draw_date.clone(),
            rules: circle.rules.clone(),
            members: circle.members.clone(),
            organisers: circle.organisers.clone(),
            draw: circle.draw.clone(),
            disallowed_draw: circle.disallowed_draw.clone(),
            draw_warnings: circle.draw_warnings.clone(),
        }
    }
}
