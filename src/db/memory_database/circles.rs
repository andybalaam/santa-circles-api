use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::data::{
    CircleInfo, DrawItem, DrawWarning, Error, Member, UsernameNick,
};
use crate::db::memory_database::Circle;

#[derive(Default, Deserialize, Serialize)]
pub struct Circles {
    #[serde(flatten)]
    circleid2circle: HashMap<String, Circle>,
}

impl Circles {
    pub fn new() -> Circles {
        Circles {
            circleid2circle: HashMap::new(),
        }
    }

    pub fn add(
        &mut self,
        circleid: String,
        circle_name: &str,
        draw_date: &str,
        rules: &str,
        members: &[Member],
        organisers: &[UsernameNick],
        draw: &[DrawItem],
        disallowed_draw: &[DrawItem],
        draw_warnings: &[DrawWarning],
    ) -> Result<CircleInfo, Error> {
        if self.circleid2circle.contains_key(&circleid) {
            Err(Error::CircleAlreadyExists)
        } else {
            let info = CircleInfo {
                circleid: circleid.clone(),
                name: String::from(circle_name),
            };
            let circle = Circle {
                info: info.clone(),
                draw_date: String::from(draw_date),
                rules: String::from(rules),
                members: members.into(),
                organisers: organisers.into(),
                draw: draw.into(),
                disallowed_draw: disallowed_draw.into(),
                draw_warnings: draw_warnings.into(),
            };
            self.circleid2circle.insert(circleid, circle);
            Ok(info)
        }
    }

    pub fn update(
        &mut self,
        circleid: &str,
        name: &str,
    ) -> Result<CircleInfo, Error> {
        self.circleid2circle
            .get_mut(circleid)
            .map(|circle| {
                circle.info.name = String::from(name);
                circle.info.clone()
            })
            .ok_or(Error::CircleDoesNotExist)
    }

    pub fn delete(&mut self, circleid: &str) -> Result<(), Error> {
        self.circleid2circle
            .remove(circleid)
            .ok_or(Error::CircleDoesNotExist)
            .map(|_| ())
    }

    pub fn get(&self, circleid: &str) -> Result<&Circle, Error> {
        self.circleid2circle
            .get(circleid)
            .ok_or(Error::CircleDoesNotExist)
    }

    pub fn get_mut(&mut self, circleid: &str) -> Result<&mut Circle, Error> {
        self.circleid2circle
            .get_mut(circleid)
            .ok_or(Error::CircleDoesNotExist)
    }

    pub fn len(&self) -> usize {
        self.circleid2circle.len()
    }

    pub fn iter(&self) -> std::collections::hash_map::Values<String, Circle> {
        self.circleid2circle.values()
    }

    pub fn iter_mut(
        &mut self,
    ) -> std::collections::hash_map::ValuesMut<String, Circle> {
        self.circleid2circle.values_mut()
    }
}
