use serde::{Deserialize, Serialize};
use std::cmp::Ordering;

use crate::data::{Error, ListInfo, ListItem, NewItem};

/***
 * Compare f64's, considering NaN less than other values.
 */
fn f64_cmp(a: &f64, b: &f64) -> Ordering {
    match (a.is_nan(), b.is_nan()) {
        (true, true) => Ordering::Equal,
        (true, false) => Ordering::Less,
        (false, true) => Ordering::Greater,
        _ => a.partial_cmp(b).unwrap(),
    }
}

#[derive(Clone, Deserialize, Serialize)]
pub struct List {
    next_id: u64,
    pub info: ListInfo,
    pub items: Vec<ListItem>,
}

impl List {
    pub fn new(info: ListInfo) -> List {
        List {
            next_id: 0,
            info,
            items: Vec::new(),
        }
    }

    pub fn add(
        &mut self,
        new_item: &NewItem,
        taken_ids: &Vec<String>,
    ) -> ListItem {
        let order = new_item.order.unwrap_or(self.next_order());
        let itemid = new_item.itemid.clone().unwrap_or(self.next_id(taken_ids));

        let item = ListItem {
            itemid: String::from(itemid),
            order,
            ticked: new_item.ticked,
            text: String::from(&new_item.text),
        };
        self.items.push(item.clone());
        self.sort();
        item
    }

    pub fn find(&self, itemid: &str) -> Result<ListItem, Error> {
        self.items
            .iter()
            .find(|item| item.itemid == itemid)
            .ok_or(Error::ItemDoesNotExist)
            .map(|item: &ListItem| item.clone())
    }

    pub fn replace(
        &mut self,
        itemid: &str,
        order: f64,
        ticked: bool,
        text: &str,
    ) -> Result<ListItem, Error> {
        let ret = self
            .items
            .iter_mut()
            .find(|item| item.itemid == itemid)
            .ok_or(Error::ItemDoesNotExist)
            .map(|item: &mut ListItem| {
                item.order = order;
                item.ticked = ticked;
                item.text = String::from(text);
                item.clone()
            });

        self.sort();

        ret
    }

    pub fn delete(&mut self, itemid: &str) -> Result<(), Error> {
        for i in 0..self.items.len() {
            if self.items[i].itemid == itemid {
                self.items.remove(i);
                self.sort();
                return Ok(());
            }
        }
        Err(Error::ItemDoesNotExist)
    }

    pub fn replace_all(&mut self, new_items: &Vec<NewItem>) -> &Vec<ListItem> {
        let new_ids: Vec<String> = new_items
            .iter()
            .map(|i| i.itemid.clone().unwrap_or(String::from("0")))
            .collect();

        self.items.clear();
        for item in new_items {
            self.add(item, &new_ids);
        }

        &self.items
    }

    fn sort(&mut self) {
        self.items
            .sort_by(|item1, item2| f64_cmp(&item1.order, &item2.order));
    }

    fn next_order(&self) -> f64 {
        self.items
            .iter()
            .map(|item| item.order)
            .max_by(f64_cmp)
            .unwrap_or(0.0)
            + 16.0
    }

    fn next_id(&self, new_ids: &Vec<String>) -> String {
        format!(
            "{:04}",
            self.items
                .iter()
                .map(|item| &item.itemid)
                .chain(new_ids.iter())
                .map(|itemid| itemid.parse::<u64>().unwrap_or(0))
                .max()
                .unwrap_or(0)
                + 1
        )
    }
}
