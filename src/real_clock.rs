use std::time::SystemTime;

use crate::clock::Clock;

pub struct RealClock {}

impl RealClock {
    pub fn new() -> RealClock {
        RealClock {}
    }
}

impl Clock for RealClock {
    fn now(&self) -> SystemTime {
        SystemTime::now()
    }
}
