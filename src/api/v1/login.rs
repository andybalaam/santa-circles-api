use actix_web::{web, HttpResponse, Responder};
use serde::Deserialize;
use std::time::Duration;

use crate::authorization;
use crate::logged_in_user::LoggedInUser;
use crate::response;
use crate::state::State;

const TOKEN_EXPIRY_REMEMBER: u64 = 60 * 60 * 24 * 21; // 30 days
const TOKEN_EXPIRY_FORGET: u64 = 60 * 60 * 24; // 24 hours

#[derive(Deserialize)]
pub struct LoginOptions {
    pub remember_me: bool,
}

#[derive(Deserialize)]
pub struct TokenToDelete {
    pub token: String,
}

#[derive(Deserialize)]
pub struct DeletePath {
    username: String,
}

pub async fn post(
    login_options: web::Json<LoginOptions>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::generate_token(&logged_in_user, &state)
        .and_then(|_| {
            let token = state.token_generator.generate_login_token();
            let expiry = state.clock.now() + expiry(login_options.remember_me);
            state.database.add_user_token(
                &logged_in_user.username,
                &token,
                expiry,
            )
        })
        .map(|user_token_response| {
            HttpResponse::Created().json(user_token_response)
        })
        .map_err(response::from_error)
}

pub async fn delete(
    path: web::Path<DeletePath>,
    token_to_delete: web::Json<TokenToDelete>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::delete_token(&path.username, &logged_in_user, &state)
        .and_then(|_| {
            state.database.delete_user_token(
                &logged_in_user.username,
                &token_to_delete.token,
            )
        })
        .map(|_| HttpResponse::NoContent())
        .map_err(response::from_error)
}

fn expiry(remember_me: bool) -> Duration {
    Duration::from_secs(if remember_me {
        TOKEN_EXPIRY_REMEMBER
    } else {
        TOKEN_EXPIRY_FORGET
    })
}
