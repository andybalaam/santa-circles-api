use actix_web::{web, HttpResponse, Responder};

use crate::authorization;
use crate::data::NewUser;
use crate::response;
use crate::state::State;

async fn post(
    mut new_user: web::Json<NewUser>,
    state: web::Data<State>,
) -> impl Responder {
    authorization::no_users_exist(&state)
        .and_then(|_| {
            new_user.admin = true;
            state.database.add_user(&new_user, None)
        })
        .map(|user_response| {
            HttpResponse::Created().json(user_response.into_no_codes())
        })
        .map_err(response::from_error)
}

// Let the UI know whether we are in setup mode
async fn get(state: web::Data<State>) -> impl Responder {
    match authorization::no_users_exist(&state) {
        Ok(_) => HttpResponse::NoContent(),
        Err(_) => HttpResponse::NotFound(),
    }
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("")
            .route(web::post().to(post))
            .route(web::get().to(get)),
    );
}
