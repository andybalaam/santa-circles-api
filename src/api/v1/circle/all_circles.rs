use actix_web::{web, HttpResponse, Responder};
use serde::Serialize;

use crate::api::v1::circle::single_circle::circle_for_request;
use crate::data::{CircleInfo, NewCircle};
use crate::logged_in_user::LoggedInUser;
use crate::state::State;
use crate::{authorization, response};

#[derive(Serialize)]
struct Circles {
    member_circles: Vec<CircleInfo>,
    organiser_circles: Vec<CircleInfo>,
}

async fn get(
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::read_own_circles(&logged_in_user, &state)
        .and_then(|_| state.database.get_circles(&logged_in_user.username))
        .map(|circles| HttpResponse::Ok().json(circles))
        .map_err(response::from_error)
}

async fn post(
    new_circle: web::Json<NewCircle>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::create_a_circle(&logged_in_user, &state)
        .and_then(|_| {
            let circle_contents = circle_for_request(new_circle, Vec::new());

            state.database.add_circle(
                &logged_in_user.username,
                &circle_contents.name,
                &circle_contents.draw_date,
                &circle_contents.rules,
                &circle_contents.members,
                &circle_contents.organisers,
                &circle_contents.draw,
                &circle_contents.disallowed_draw,
                &circle_contents.draw_warnings,
            )
        })
        .map(|circle_info: CircleInfo| {
            HttpResponse::Created().json(circle_info)
        })
        .map_err(response::from_error)
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("")
            .route(web::get().to(get))
            .route(web::post().to(post)),
    );
}
