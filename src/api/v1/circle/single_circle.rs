use actix_web::{web, HttpResponse, Responder};
use serde::{Deserialize, Serialize};

use crate::data::new_member::NewMember;
use crate::data::{CircleContents, Member, NewCircle, WishlistLink};
use crate::draw_calculation::calculate_warnings;
use crate::logged_in_user::LoggedInUser;
use crate::response;
use crate::state::State;
use crate::{authorization, draw_calculation};

#[derive(Deserialize)]
struct Path {
    circleid: String,
}

#[derive(Deserialize)]
struct Query {
    view: Option<String>,
}

async fn get(
    path: web::Path<Path>,
    query: web::Query<Query>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    if query.view == Some(String::from("organise")) {
        authorization::modify_circle(&path.circleid, &logged_in_user, &state)
            .and_then(|_| {
                state.database.get_circle_organiser_info(&path.circleid)
            })
            .map(|circle_info| HttpResponse::Ok().json(circle_info))
            .map_err(response::from_error)
    } else {
        authorization::view_circle(&path.circleid, &logged_in_user, &state)
            .and_then(|_| {
                state.database.get_circle_member_info(
                    &path.circleid,
                    &logged_in_user.username,
                )
            })
            .map(|circle_info| HttpResponse::Ok().json(circle_info))
            .map_err(response::from_error)
    }
}

async fn put(
    path: web::Path<Path>,
    updated_circle: web::Json<NewCircle>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::modify_circle(&path.circleid, &logged_in_user, &state)
        .and_then(|_| {
            let prev_members = state
                .database
                .get_circle_member_info(
                    &path.circleid,
                    &logged_in_user.username,
                )
                .map(|info| info.members.clone())
                .unwrap_or(Vec::new());
            let circle_contents =
                circle_for_request(updated_circle, prev_members);
            state.database.update_circle(
                &path.circleid,
                &circle_contents.name,
                &circle_contents.draw_date,
                &circle_contents.rules,
                &circle_contents.members,
                &circle_contents.organisers,
                &circle_contents.draw,
                &circle_contents.disallowed_draw,
                &circle_contents.draw_warnings,
            )
        })
        .map(|circle_info| HttpResponse::Ok().json(circle_info))
        .map_err(response::from_error)
}

async fn delete(
    path: web::Path<Path>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::delete_circle(&path.circleid, &logged_in_user, &state)
        .and_then(|_| state.database.delete_circle(&path.circleid))
        .map(|_| HttpResponse::NoContent())
        .map_err(response::from_error)
}

fn fill_in_wishlist(
    new_members: Vec<NewMember>,
    old_members: Vec<Member>,
) -> Vec<Member> {
    new_members
        .into_iter()
        .map(|mem| {
            let (wishlist_text, wishlist_links) = member_wishlist(
                &mem.username,
                mem.wishlist_text,
                mem.wishlist_links,
                &old_members,
            );
            Member {
                username: mem.username,
                nickname: mem.nickname,
                wishlist_text,
                wishlist_links,
            }
        })
        .collect()
}

fn member_wishlist(
    username: &str,
    wishlist_text: Option<String>,
    wishlist_links: Option<Vec<WishlistLink>>,
    old_members: &Vec<Member>,
) -> (String, Vec<WishlistLink>) {
    match (wishlist_text, wishlist_links) {
        (Some(t), Some(l)) => return (t, l),
        (t, l) => {
            let old_mem = old_members.iter().find(|o| o.username == username);
            if let Some(old_mem) = old_mem {
                (
                    t.unwrap_or(old_mem.wishlist_text.clone()),
                    l.unwrap_or(old_mem.wishlist_links.clone()),
                )
            } else {
                (t.unwrap_or(String::from("")), l.unwrap_or(Vec::new()))
            }
        }
    }
}

pub fn circle_for_request(
    req_circle: web::Json<NewCircle>,
    prev_members: Vec<Member>,
) -> CircleContents {
    let c: NewCircle = req_circle.0;
    let members = fill_in_wishlist(c.members, prev_members);
    if c.request_draw.unwrap_or(false) {
        let mut outcome = draw_calculation::calculate(
            &members,
            &c.disallowed_draw,
            c.deterministic_draw.unwrap_or(false),
        );

        // Ensure the draw order does not give away the draw
        outcome.draw.sort();

        CircleContents {
            name: c.name,
            draw_date: c.draw_date,
            rules: c.rules,
            members,
            organisers: c.organisers,
            draw: outcome.draw,
            disallowed_draw: c.disallowed_draw,
            draw_warnings: outcome.draw_warnings,
        }
    } else {
        let draw_warnings =
            calculate_warnings(&members, &c.disallowed_draw, &c.draw);

        CircleContents {
            name: c.name,
            draw_date: c.draw_date,
            rules: c.rules,
            members,
            organisers: c.organisers,
            draw: c.draw,
            disallowed_draw: c.disallowed_draw,
            draw_warnings,
        }
    }
}

#[derive(Deserialize)]
struct WishlistPath {
    circleid: String,
    username: String,
}

#[derive(Serialize)]
struct WishlistInfo {
    circleid: String,
    username: String,
}

#[derive(Deserialize)]
struct NewWishlist {
    wishlist_text: String,
    wishlist_links: Vec<WishlistLink>,
}

async fn put_wishlist(
    path: web::Path<WishlistPath>,
    updated_wishlist: web::Json<NewWishlist>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::modify_wishlist(
        &path.circleid,
        &path.username,
        &logged_in_user,
        &state,
    )
    .and_then(|_| {
        let mut circle =
            state.database.get_circle_organiser_info(&path.circleid)?;

        circle
            .members
            .iter_mut()
            .find(|m| m.username == path.username)
            .map(|m| {
                m.wishlist_text = updated_wishlist.wishlist_text.clone();
                m.wishlist_links = updated_wishlist.wishlist_links.clone();
            });

        state.database.update_circle(
            &path.circleid,
            &circle.name,
            &circle.draw_date,
            &circle.rules,
            &circle.members,
            &circle.organisers,
            &circle.draw,
            &circle.disallowed_draw,
            &circle.draw_warnings,
        )?;

        Ok(WishlistInfo {
            circleid: String::from(&path.circleid),
            username: String::from(&path.username),
        })
    })
    .map(|wishlist_info| HttpResponse::Ok().json(wishlist_info))
    .map_err(response::from_error)
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/{circleid}")
            .route(web::get().to(get))
            .route(web::put().to(put))
            .route(web::delete().to(delete)),
    )
    .service(
        web::resource("/{circleid}/members/{username}/wishlist")
            .route(web::put().to(put_wishlist)),
    );
}
