use actix_web::{web, HttpResponse, Responder};
use log::debug;
use serde::{Deserialize, Serialize};

use crate::data::{Error, NewUser};
use crate::response;
use crate::state::State;

#[derive(Deserialize, Serialize)]
pub struct NewUnregisteredUser {
    pub username: String,
    pub password: String,
}

#[derive(Deserialize, Serialize)]
struct RegistrationCode {
    registration_code: String,
}

/// A registration code exists but we are obfuscating it because we are
/// capable of sending email so don't need to reveal it.
#[derive(Deserialize, Serialize)]
struct RegistrationCodeWithoutCode {
    // Don't write if it's false - should never happen
    #[serde(skip_serializing_if = "std::ops::Not::not")]
    registration_code: bool,
}

#[derive(Deserialize)]
struct Path {
    username: String,
}

async fn post(
    new_user: web::Json<NewUnregisteredUser>,
    state: web::Data<State>,
) -> impl Responder {
    // TODO: Maybe a constructor for NewUser, with a Result that we map on here?
    let new_user = NewUser {
        username: String::from(&new_user.username),
        password: String::from(&new_user.password),
        admin: false,
    };
    if let Some(problem) = user_check(&new_user) {
        return Err(response::from_error(Error::UserDetailsInvalid(problem)));
    }

    let registration_code = state.token_generator.generate_registration_code();

    state
        .emailer
        .send_registration_code(&new_user.username, &registration_code)
        .map_err(response::from_error)?;

    let user_response = state
        .database
        .add_user(&new_user, Some(registration_code))
        .map_err(response::from_error)?;

    if state.emailer.can_send_email() {
        Ok(HttpResponse::Created()
            .json(user_response.into_no_registration_code()))
    } else {
        Ok(HttpResponse::Created()
            .json(user_response.into_with_registration_code()))
    }
}

fn user_check(new_user: &NewUser) -> Option<&'static str> {
    if new_user.username.contains(' ') {
        return Some("Invalid username - cannot contain spaces");
    }
    None // User details are fine
}

async fn get(path: web::Path<Path>, state: web::Data<State>) -> impl Responder {
    // Note: no authorization!
    debug!("Registration code requested.");
    let registration_code =
        state
            .database
            .get_user(&path.username)
            .and_then(|user_response| {
                user_response
                    .registration_code
                    .ok_or(Error::UserIsAlreadyRegistered)
            });

    match registration_code {
        Ok(registration_code) => {
            if state.emailer.can_send_email() {
                HttpResponse::Ok().json(RegistrationCodeWithoutCode {
                    registration_code: true,
                })
            } else {
                HttpResponse::Ok().json(RegistrationCode { registration_code })
            }
        }
        Err(e) => response::from_error(e),
    }
}

/// Register this user by supplying the registration code send via email.
async fn put(
    path: web::Path<Path>,
    registration_code: web::Json<RegistrationCode>,
    state: web::Data<State>,
) -> impl Responder {
    // Note: no authorization!
    debug!("Registration request received.");
    let user_code =
        state
            .database
            .get_user(&path.username)
            .and_then(|user_response| {
                debug!("User exists.");
                user_response
                    .registration_code
                    .ok_or(Error::UserIsAlreadyRegistered)
            });

    user_code
        .and_then(|user_code| {
            if user_code == registration_code.registration_code {
                debug!("Correct registration code.");
                state
                    .database
                    .set_user_registered(&path.username)
                    .map(|_| HttpResponse::NoContent())
            } else {
                debug!(
                    "Incorrect registration code: {} != {}.",
                    user_code, registration_code.registration_code
                );
                Err(Error::IncorrectRegistrationCode)
            }
        })
        .map_err(response::from_error)
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(web::resource("").route(web::post().to(post)))
        .service(
            web::resource("{username}")
                .route(web::get().to(get))
                .route(web::put().to(put)),
        );
}
