use actix_web::{web, HttpResponse};
use log::debug;
use serde::Deserialize;

use crate::data::user_response::UserResponseJustUsername;
use crate::data::{Error, UpdateUser};
use crate::response;
use crate::state::State;

#[derive(Deserialize)]
struct Path {
    username: String,
}

#[derive(Deserialize)]
struct DoResetPassword {
    password_reset_code: String,
    new_password: String,
}

/// Given the username in the path, and the reset code and new password in the
/// body, change this user's password without requiring authentication.
async fn post(
    path: web::Path<Path>,
    do_reset_password: web::Json<DoResetPassword>,
    state: web::Data<State>,
) -> Result<HttpResponse, HttpResponse> {
    // Note: no authorization!
    debug!("Do-reset request received.");

    let user_response = state
        .database
        .get_user(&path.username)
        .map_err(response::from_error)?;

    debug!("User exists.");

    let reset_code = user_response
        .password_reset_code
        .ok_or(Error::PasswordResetHasNotBeenRequested)
        .map_err(response::from_error)?;

    debug!("There is a request to reset password.");

    if reset_code != do_reset_password.password_reset_code {
        debug!(
            "Incorrect reset code supplied: {}.",
            do_reset_password.password_reset_code
        );
        return Err(response::from_error(Error::IncorrectPasswordResetCode));
    }

    debug!("Correct reset code supplied.");
    state
        .database
        .update_user(
            &path.username,
            &UpdateUser {
                password: Some(do_reset_password.new_password.clone()),
                admin: None,
                reset_code: Some(None),
            },
        )
        .map_err(response::from_error)?;

    // If we successfully did a password reset, this counts as registration
    // since it proves we can receive emails.
    state
        .database
        .set_user_registered(&path.username)
        .map_err(response::from_error)?;

    Ok(HttpResponse::Ok().json(UserResponseJustUsername {
        username: path.username.clone(),
    }))
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(web::resource("{username}").route(web::post().to(post)));
}
