use actix_web::{web, HttpResponse};
use log::debug;
use serde::Deserialize;

use crate::data::UpdateUser;
use crate::response;
use crate::state::State;

#[derive(Deserialize)]
struct Path {
    username: String,
}

async fn post(
    path: web::Path<Path>,
    state: web::Data<State>,
) -> Result<HttpResponse, HttpResponse> {
    // Note: no authorization!
    debug!("Reset request received.");

    let reset_code = state.token_generator.generate_password_reset_code();

    let user_res = state
        .database
        .update_user(
            &path.username,
            &UpdateUser {
                password: None,
                admin: None,
                reset_code: Some(Some(reset_code.clone())),
            },
        )
        .map_err(response::from_error)?;

    state
        .emailer
        .send_password_reset_code(&path.username, &reset_code)
        .map_err(response::from_error)?;

    Ok(HttpResponse::Ok().json(user_res.into_just_username()))
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(web::resource("{username}").route(web::post().to(post)));
}
