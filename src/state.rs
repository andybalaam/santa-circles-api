use crate::clock::Clock;
use crate::db::Database;
use crate::emailer::Emailer;
use crate::privacy_page::PrivacyPage;
use crate::token_generator::TokenGenerator;

pub struct State {
    pub database: Box<dyn Database>,
    pub token_generator: Box<dyn TokenGenerator>,
    pub clock: Box<dyn Clock>,
    pub emailer: Box<dyn Emailer>,
    pub privacy_page: PrivacyPage,
}

impl State {
    pub fn new(
        database: Box<dyn Database>,
        token_generator: Box<dyn TokenGenerator>,
        clock: Box<dyn Clock>,
        emailer: Box<dyn Emailer>,
        privacy_page: PrivacyPage,
    ) -> State {
        State {
            database,
            token_generator,
            clock,
            emailer,
            privacy_page,
        }
    }
}
