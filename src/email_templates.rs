use std::fs::File;
use std::io::Read;

#[derive(Clone)]
pub struct EmailTemplates {
    pub registration_contents: String,
    pub password_reset_contents: String,
}

const DEFAULT_REGISTRATIONS: &str = r#";
Your registration code is '${registration_code}'

This instance of Santa Circles does not have its email templates set up
properly, so if you were not expecting this message, please reply
asking to unsubscribe.

If that doesn't work, you can try contacting the admins of the main Santa
Circles instance at https://santacircles.artificialworlds.net/contact
but bear in mind this email may have been sent by someone completely
unrelated to them.
"#;

const DEFAULT_PASSWORD_RESET: &str = r#";
Your password reset code is '${registration_code}'

This instance of Santa Circles does not have its email templates set up
properly, so if you were not expecting this message, please reply
asking to unsubscribe.

If that doesn't work, you can try contacting the admins of the main Santa
Circles instance at https://santacircles.artificialworlds.net/contact
but bear in mind this email may have been sent by someone completely
unrelated to them.
"#;

impl EmailTemplates {
    pub fn new(
        registration_contents: String,
        password_reset_contents: String,
    ) -> EmailTemplates {
        EmailTemplates {
            registration_contents,
            password_reset_contents,
        }
    }

    pub fn load(
        registration_filename: &str,
        password_reset_filename: &str,
    ) -> EmailTemplates {
        let registration_contents =
            Self::load_file(registration_filename, DEFAULT_REGISTRATIONS);

        let password_reset_contents =
            Self::load_file(password_reset_filename, DEFAULT_PASSWORD_RESET);

        EmailTemplates::new(registration_contents, password_reset_contents)
    }

    fn load_file(filename: &str, default_contents: &str) -> String {
        let file = File::open(filename);
        if let Ok(mut file) = file {
            let mut contents = String::new();
            let res = file.read_to_string(&mut contents);
            if let Ok(_) = res {
                println!("Loaded email template from {}", filename);
                return contents;
            }
        }
        println!(
            "Unable to load {} - please create it and ensure it is readable",
            filename
        );
        default_contents.to_owned()
    }

    pub fn registration(&self, registration_code: &str) -> String {
        self.registration_contents
            .replace("${registration_code}", registration_code)
    }

    pub(crate) fn password_reset(&self, reset_code: &str) -> String {
        self.password_reset_contents
            .replace("${reset_code}", reset_code)
    }
}
