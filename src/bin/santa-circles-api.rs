use actix_cors::{Cors, CorsFactory};
use actix_web::{middleware, web, App, HttpServer};

use clap::{command, Parser};
use santa_circles_api;
use santa_circles_api::db::memory_database::MemoryDatabase;
use santa_circles_api::email_templates::EmailTemplates;
use santa_circles_api::emailer::Emailer;
use santa_circles_api::privacy_page::PrivacyPage;
use santa_circles_api::real_clock::RealClock;
use santa_circles_api::real_emailer::{DummyEmailer, RealEmailer};
use santa_circles_api::real_idgen::RealIdGen;
use santa_circles_api::real_password_handler::RealPasswordHandler;
use santa_circles_api::real_token_generator::RealTokenGenerator;
use santa_circles_api::save::FileSaver;
use santa_circles_api::state::State;

const REAL_PASSWORD_HANDLER: RealPasswordHandler = RealPasswordHandler {};

#[derive(Parser, Debug)]
#[command(
    author,
    version,
    about,
    long_about = "A server for storing secret santa information.\n\
        See https://gitlab.com/andybalaam/santa-circles-api/\n\
        \n\
        Data is stored in the local directory, in the file state.json."
)]
struct Args {
    /// Alternate bind address [default: all interfaces]
    #[arg(long, env, default_value = "0.0.0.0")]
    bind: String,

    /// Alternate port to listen on
    #[arg(env, default_value = "8088")]
    port: String,

    /// An email server to send messages via SMTP
    #[arg(long, env)]
    email_relay: Option<String>,

    /// A username to use with the email relay
    #[arg(long, env)]
    email_username: Option<String>,

    /// A password to use with the email relay
    #[arg(long, env)]
    email_password: Option<String>,

    /// Specify the from address to use when sending emails
    #[arg(long, env)]
    email_from_address: Option<String>,

    /// An address to cc when sending emails
    #[arg(long, env)]
    email_cc_address: Option<String>,
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    let privacy_page = PrivacyPage::load("privacy.html");
    let email_templates = EmailTemplates::load(
        "registration_email.txt",
        "password_reset_email.txt",
    );
    let args = Args::parse();
    let file_saver = FileSaver::new("state.json");
    let mem_db = MemoryDatabase::new(
        Box::new(file_saver.clone()),
        &REAL_PASSWORD_HANDLER,
        Box::new(RealIdGen::new()),
    );
    let token_generator = RealTokenGenerator::new();
    let clock = RealClock::new();

    let emailer: Box<dyn Emailer> = match RealEmailer::new(
        email_templates,
        args.email_relay,
        args.email_username,
        args.email_password,
        args.email_from_address,
        args.email_cc_address,
    ) {
        Ok(emailer) => Box::new(emailer),
        Err(error) => {
            println!("{}", error);
            Box::new(DummyEmailer::new())
        }
    };

    let data = web::Data::new(State::new(
        Box::new(mem_db),
        Box::new(token_generator),
        Box::new(clock),
        emailer,
        privacy_page,
    ));

    println!("Listening on http://{}:{}", args.bind, args.port);
    let res = HttpServer::new(move || {
        App::new()
            .wrap(cors())
            .wrap(middleware::NormalizePath)
            .app_data(data.clone())
            .wrap(middleware::Logger::default())
            .configure(santa_circles_api::config_unauthenticated)
            .configure(santa_circles_api::config)
    })
    .bind(format!("{}:{}", args.bind, args.port))?
    .run()
    .await;

    file_saver.quit();

    res
}

fn cors() -> CorsFactory {
    Cors::new().max_age(3600).finish()
}
