use serde::Serialize;

#[derive(Serialize)]
pub struct UserTokenResponse {
    pub username: String,
    pub token: String,
}
