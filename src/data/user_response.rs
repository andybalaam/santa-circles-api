use serde::Serialize;

// Not Serialize, because it contains codes we normally shouldn't return.
// (registration_code is only returned if we can't email so we have to,
// password_reset_code is never returned.)
pub struct UserResponse {
    pub username: String,
    pub admin: bool,
    pub registration_code: Option<String>,
    pub password_reset_code: Option<String>,
}

impl UserResponse {
    /// Since we can send email, strip the user response of its registration code
    pub fn into_no_registration_code(
        self,
    ) -> UserResponseWithoutRegistrationCode {
        UserResponseWithoutRegistrationCode {
            username: self.username,
            admin: self.admin,
            registration_code: self.registration_code.is_some(),
        }
    }

    pub fn into_with_registration_code(
        self,
    ) -> UserResponseWithRegistrationCode {
        UserResponseWithRegistrationCode {
            username: self.username,
            admin: self.admin,
            registration_code: self.registration_code,
        }
    }

    pub fn into_just_username(self) -> UserResponseJustUsername {
        UserResponseJustUsername {
            username: self.username,
        }
    }

    pub fn into_no_codes(self) -> UserResponseNoCodes {
        UserResponseNoCodes {
            username: self.username,
            admin: self.admin,
        }
    }
}

#[derive(Serialize)]
pub struct UserResponseWithoutRegistrationCode {
    pub username: String,
    pub admin: bool,

    // Don't write if it's false
    #[serde(skip_serializing_if = "std::ops::Not::not")]
    pub registration_code: bool,
}

#[derive(Serialize)]
pub struct UserResponseWithRegistrationCode {
    pub username: String,
    pub admin: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub registration_code: Option<String>,
}

#[derive(Serialize)]
pub struct UserResponseJustUsername {
    pub username: String,
}

#[derive(Serialize)]
pub struct UserResponseNoCodes {
    pub username: String,
    pub admin: bool,
}
