use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct UsernameNick {
    pub username: String,
    pub nickname: String,
}
