use serde::{Deserialize, Serialize};

use crate::data::DrawItem;
use crate::data::Member;
use crate::data::UsernameNick;

use super::DrawWarning;

#[derive(Clone, Deserialize, Serialize)]
pub struct CircleOrganiserInfo {
    pub circleid: String,
    pub name: String,
    pub draw_date: String,
    pub rules: String,
    pub members: Vec<Member>,
    pub organisers: Vec<UsernameNick>,
    pub draw: Vec<DrawItem>,
    pub disallowed_draw: Vec<DrawItem>,
    pub draw_warnings: Vec<DrawWarning>,
}
