use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct WishlistLink {
    pub text: String,
    pub url: String,
}
