use serde::{Deserialize, Serialize};

use crate::data::WishlistLink;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct NewMember {
    pub username: String,
    pub nickname: String,
    pub wishlist_text: Option<String>,
    pub wishlist_links: Option<Vec<WishlistLink>>,
}
