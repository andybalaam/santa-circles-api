use serde::{Deserialize, Serialize};

use crate::data::circle_info::CircleInfo;

#[derive(Clone, Deserialize, Serialize)]
pub struct CirclesInfo {
    pub username: String,
    pub member_circles: Vec<CircleInfo>,
    pub organiser_circles: Vec<CircleInfo>,
}
