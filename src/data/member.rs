use serde::{Deserialize, Serialize};

use crate::data::WishlistLink;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Member {
    pub username: String,
    pub nickname: String,
    pub wishlist_text: String,
    pub wishlist_links: Vec<WishlistLink>,
}
