use serde::Deserialize;

use crate::data::{DrawItem, DrawWarning, Member, UsernameNick};

#[derive(Deserialize)]
pub struct CircleContents {
    pub name: String,
    pub draw_date: String,
    pub rules: String,
    pub members: Vec<Member>,
    pub organisers: Vec<UsernameNick>,
    pub draw: Vec<DrawItem>,
    pub disallowed_draw: Vec<DrawItem>,
    pub draw_warnings: Vec<DrawWarning>,
}
