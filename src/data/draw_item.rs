use serde::{Deserialize, Serialize};

#[derive(
    Clone, Debug, Deserialize, Eq, Ord, PartialEq, PartialOrd, Serialize,
)]
pub struct DrawItem {
    pub from_username: String,
    pub to_username: String,
}

impl DrawItem {
    pub fn new(from: &str, to: &str) -> Self {
        Self {
            from_username: String::from(from),
            to_username: String::from(to),
        }
    }
}
