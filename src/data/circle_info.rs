use serde::{Deserialize, Serialize};

// TODO: rename all *Info to something like *NameId

#[derive(Clone, Deserialize, Serialize)]
pub struct CircleInfo {
    pub circleid: String,
    pub name: String,
}
