use serde::{Deserialize, Serialize};

use crate::data::DrawItem;

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub enum DrawWarning {
    BreaksRestriction(DrawItem),
    EmptyFrom(String),
    EmptyTo(String),
    GivingMultiple(String, Vec<String>),
    NoUsername(String),
    NotGiving(String),
    NotReceiving(String),
    ReceivingMultiple(String, Vec<String>),
}
