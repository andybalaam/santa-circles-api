use serde::Deserialize;

use crate::data::{DrawItem, NewMember, UsernameNick};

#[derive(Deserialize)]
pub struct NewCircle {
    pub name: String,
    pub draw_date: String,
    pub rules: String,
    pub members: Vec<NewMember>,
    pub organisers: Vec<UsernameNick>,
    pub request_draw: Option<bool>,
    pub deterministic_draw: Option<bool>,
    pub draw: Vec<DrawItem>,
    pub disallowed_draw: Vec<DrawItem>,
}
