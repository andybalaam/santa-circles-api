use serde::{Deserialize, Serialize};

use crate::data::{Member, UsernameNick};

#[derive(Clone, Deserialize, Serialize)]
pub struct CircleMemberInfo {
    pub circleid: String,
    pub name: String,
    pub draw_date: String,
    pub rules: String,
    pub members: Vec<Member>,
    pub organisers: Vec<UsernameNick>,
    pub recipients: Option<Vec<String>>,
}
