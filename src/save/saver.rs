pub trait Saver<T>: Send + Sync {
    fn save(&self, t: &T);
    fn load(&self) -> T;
}
