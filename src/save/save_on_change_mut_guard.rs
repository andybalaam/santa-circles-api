use std::ops::{Deref, DerefMut};
use std::sync::MutexGuard;

use crate::save::Saver;

pub struct SaveOnChangeMutGuard<'a, T> {
    mutex_guard: MutexGuard<'a, T>,
    saver: &'a dyn Saver<T>,
}

impl<'a, T> SaveOnChangeMutGuard<'a, T> {
    pub fn new(
        mutex_guard: MutexGuard<'a, T>,
        saver: &'a dyn Saver<T>,
    ) -> SaveOnChangeMutGuard<'a, T> {
        SaveOnChangeMutGuard { mutex_guard, saver }
    }
}

impl<'a, T> Drop for SaveOnChangeMutGuard<'a, T> {
    fn drop(&mut self) {
        self.saver.save(self.mutex_guard.deref())
    }
}

impl<T> Deref for SaveOnChangeMutGuard<'_, T> {
    type Target = T;

    fn deref(&self) -> &T {
        self.mutex_guard.deref()
    }
}

impl<T> DerefMut for SaveOnChangeMutGuard<'_, T> {
    fn deref_mut(&mut self) -> &mut T {
        self.mutex_guard.deref_mut()
    }
}
