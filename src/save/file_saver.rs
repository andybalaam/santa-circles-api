use serde::de::DeserializeOwned;
use serde::Serialize;
use std::fs::File;
use std::io::ErrorKind;
use std::sync::mpsc::Sender;
use std::sync::{Arc, Mutex};
use std::thread::JoinHandle;

use crate::save::{Saver, SaverThread, SaverThreadMessage};

#[derive(Clone)]
pub struct FileSaver {
    saver_thread_sender: Arc<Mutex<Sender<SaverThreadMessage>>>,
    saver_thread_join_handle: Arc<Mutex<Option<JoinHandle<()>>>>,
    filename: String,
}

impl FileSaver {
    pub fn new(filename: &str) -> FileSaver {
        let (saver_thread_sender, saver_thread_join_handle) =
            SaverThread::spawn(String::from(filename));

        FileSaver {
            saver_thread_sender: Arc::new(Mutex::new(saver_thread_sender)),
            saver_thread_join_handle: Arc::new(Mutex::new(Some(
                saver_thread_join_handle,
            ))),
            filename: String::from(filename),
        }
    }

    pub fn quit(&self) {
        self.saver_thread_sender
            .lock()
            .unwrap()
            .send(SaverThreadMessage::Quit)
            .expect(
                "Unable to send Quit signal to SaverThread - \
                has it been shut down already?",
            );

        self.saver_thread_join_handle
            .lock()
            .unwrap()
            .take()
            .expect(
                "Saver thread has already been joined - \
                did you call FileSaver::quit twice?",
            )
            .join()
            .expect("An error occurred while waiting for state to save.");
    }
}

impl<T: Default + DeserializeOwned + Serialize> Saver<T> for FileSaver {
    fn save(&self, users: &T) {
        let json = serde_json::to_string(users).unwrap();
        self.saver_thread_sender
            .lock()
            .unwrap()
            .send(SaverThreadMessage::InfoToSave(json))
            .unwrap();
    }

    fn load(&self) -> T {
        let f = File::open(&self.filename);
        match f {
            Ok(f) => match serde_json::from_reader(f) {
                Ok(users) => {
                    println!("Loaded state file '{}'", self.filename);
                    users
                }
                Err(e) => {
                    panic!(
                        "Error loading state file '{}': {}",
                        &self.filename, e
                    );
                }
            },
            Err(e) => match e.kind() {
                ErrorKind::NotFound => {
                    println!(
                        "State file '{} not found - it will be created.",
                        &self.filename
                    );
                    T::default()
                }
                _ => {
                    panic!(
                        "Failed to load state file '{}': {}",
                        &self.filename, e
                    );
                }
            },
        }
    }
}
