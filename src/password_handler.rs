pub trait PasswordHandler: Send + Sync {
    fn hash_password(&self, password: &str) -> String;

    fn is_correct_password(
        &self,
        possible_password: &str,
        user_hashed_password: &str,
    ) -> bool;
}
