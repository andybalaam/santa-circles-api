use crate::data::Error;
use crate::logged_in_user::LoggedInUser;
use crate::state::State;

// Special case for bootstrapping - setup is allowed only
// if there are no users at all.
pub fn no_users_exist(state: &State) -> Result<(), Error> {
    if state.database.count_users() == 0 {
        Ok(())
    } else {
        Err(Error::SetupMayOnlyBeRunOnce)
    }
}

pub fn generate_token(
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // All registered users are allowed to generate a login token
    allowed_if(is_registered(logged_in_user, state))
}

pub fn delete_token(
    token_owner_username: &String,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // Only you can log yourself out
    allowed_if(
        is_registered(logged_in_user, state)
            && logged_in_user.username == *token_owner_username,
    )
}

pub fn modify_set_of_users(
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // Admins can change users list
    allowed_if(
        is_registered(logged_in_user, state) && is_admin(logged_in_user, state),
    )
}

pub fn modify_user(
    username_to_modify: &String,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // Admins and yourself can modify you
    allowed_if(
        is_registered(logged_in_user, state)
            && (logged_in_user.username == *username_to_modify
                || is_admin(logged_in_user, state)),
    )
}

pub fn modify_user_admin_status(
    _username_to_modify: &String,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // Only admins can modify admin status
    allowed_if(
        is_registered(logged_in_user, state) && is_admin(logged_in_user, state),
    )
}

pub fn delete_user(
    username_to_delete: &String,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // Admins and yourself can delete you
    allowed_if(
        is_registered(logged_in_user, state)
            && (logged_in_user.username == *username_to_delete
                || is_admin(logged_in_user, state)),
    )
}

pub fn read_user(
    username_to_read: &String,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // Admins and yourself can find out about you
    allowed_if(
        is_registered(logged_in_user, state)
            && (logged_in_user.username == *username_to_read
                || is_admin(logged_in_user, state)),
    )
}

pub fn read_own_circles(
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // You are allowed to read your own circles
    allowed_if(is_registered(logged_in_user, state))
}

pub fn create_a_circle(
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // You are allowed to modify your own circles
    allowed_if(is_registered(logged_in_user, state))
}

pub fn delete_circle(
    circleid: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // Only organisers can delete circles
    if is_registered(logged_in_user, state)
        && is_organiser(circleid, logged_in_user, state)
    {
        Ok(())
    } else {
        // If it doesn't exist, or you're not allowed to see it, we say
        // it doesn't exist.
        if is_member(circleid, logged_in_user, state) {
            Err(Error::Unauthorized)
        } else {
            Err(Error::CircleDoesNotExist)
        }
    }
}

pub fn modify_circle(
    circleid: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // Only organisers can modify circles
    if is_registered(logged_in_user, state)
        && is_organiser(circleid, logged_in_user, state)
    {
        Ok(())
    } else {
        // If it doesn't exist, or you're not allowed to see it, we say
        // it doesn't exist.
        if is_member(circleid, logged_in_user, state) {
            Err(Error::Unauthorized)
        } else {
            Err(Error::CircleDoesNotExist)
        }
    }
}

pub fn view_circle(
    circleid: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // You are allowed to view any circle you are a member of or organise
    if is_registered(logged_in_user, state)
        && (is_member_or_organiser(circleid, logged_in_user, state))
    {
        Ok(())
    } else {
        // If it doesn't exist, or you're not allowed to see it, we say
        // it doesn't exist.
        if is_member(circleid, logged_in_user, state) {
            Err(Error::Unauthorized)
        } else {
            Err(Error::CircleDoesNotExist)
        }
    }
}

pub fn modify_wishlist(
    circleid: &str,
    username: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    let is_mem = is_member(circleid, logged_in_user, state);

    // Members can modify their own wishlists
    if is_registered(logged_in_user, state)
        && is_mem
        && logged_in_user.username == username
    {
        Ok(())
    } else {
        // If it doesn't exist, or you're not allowed to see it, we say
        // it doesn't exist.
        if is_mem {
            Err(Error::Unauthorized)
        } else {
            Err(Error::CircleDoesNotExist)
        }
    }
}

pub fn read_set_of_lists(
    lists_owner_username: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // You are allowed to list your own lists
    allowed_if(
        is_registered(logged_in_user, state)
            && logged_in_user.username == lists_owner_username,
    )
}

pub fn modify_set_of_lists(
    lists_owner_username: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // You are allowed to modify your own lists
    allowed_if(
        is_registered(logged_in_user, state)
            && logged_in_user.username == lists_owner_username,
    )
}

pub fn delete_list(
    lists_owner_username: &str,
    _listid: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // You are allowed to modify your own lists
    allowed_if(
        is_registered(logged_in_user, state)
            && logged_in_user.username == lists_owner_username,
    )
}

pub fn modify_list(
    lists_owner_username: &str,
    _listid: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // You are allowed to modify your own list
    allowed_if(
        is_registered(logged_in_user, state)
            && logged_in_user.username == lists_owner_username,
    )
}

pub fn view_list(
    lists_owner_username: &str,
    _listid: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // You are allowed to view your own list
    allowed_if(
        is_registered(logged_in_user, state)
            && logged_in_user.username == lists_owner_username,
    )
}

fn allowed_if(condition: bool) -> Result<(), Error> {
    if condition {
        Ok(())
    } else {
        Err(Error::Unauthorized)
    }
}

fn is_admin(logged_in_user: &LoggedInUser, state: &State) -> bool {
    state
        .database
        .get_user(&logged_in_user.username)
        .map(|user_response| user_response.admin)
        .unwrap_or(false) // Any error means not an admin
}

fn is_registered(logged_in_user: &LoggedInUser, state: &State) -> bool {
    state
        .database
        .get_user(&logged_in_user.username)
        .map(|user_response| user_response.registration_code.is_none())
        .unwrap_or(false) // Any error means not registered
}

fn is_member(
    circleid: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> bool {
    state
        .database
        .get_circle_member_info(circleid, &logged_in_user.username)
        .map(|circle| {
            circle
                .members
                .iter()
                .any(|un| un.username == logged_in_user.username)
        })
        .unwrap_or(false)
}

fn is_organiser(
    circleid: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> bool {
    state
        .database
        .get_circle_member_info(circleid, &logged_in_user.username)
        .map(|circle| {
            circle
                .organisers
                .iter()
                .any(|un| un.username == logged_in_user.username)
        })
        .unwrap_or(false)
}

fn is_member_or_organiser(
    circleid: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> bool {
    state
        .database
        .get_circle_member_info(circleid, &logged_in_user.username)
        .map(|circle| {
            circle
                .members
                .iter()
                .any(|un| un.username == logged_in_user.username)
                || circle
                    .organisers
                    .iter()
                    .any(|un| un.username == logged_in_user.username)
        })
        .unwrap_or(false)
}
